-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: compraemconta
-- ------------------------------------------------------
-- Server version	5.7.15-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbbairro`
--

DROP TABLE IF EXISTS `tbbairro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbbairro` (
  `idBairro` int(11) NOT NULL AUTO_INCREMENT,
  `nomeBairro` varchar(50) NOT NULL,
  `idCidade` int(11) NOT NULL,
  PRIMARY KEY (`idBairro`),
  KEY `fk_tbBairro_tbCidade1_idx` (`idCidade`),
  CONSTRAINT `fk_tbBairro_tbCidade1` FOREIGN KEY (`idCidade`) REFERENCES `tbcidade` (`idCidade`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbbairro`
--

LOCK TABLES `tbbairro` WRITE;
/*!40000 ALTER TABLE `tbbairro` DISABLE KEYS */;
INSERT INTO `tbbairro` VALUES (1,'Águas Claras',1),(2,'Brasília',2),(3,'Brazlândia',3),(4,'Candangolândia',4),(5,'Ceilândia',5),(6,'Cruzeiro',6),(7,'Fercal',7),(8,'Gama',8),(9,'Guará',9),(10,'Itapoã',10),(11,'Jardim Botânico',11),(12,'Lago Norte',12),(13,'Lago Sul',13),(14,'Núcleo Bandeirante',14),(15,'Paranoá',15),(16,'Park Way',16),(17,'Planaltina',17),(18,'Recanto das Emas',18),(19,'Riacho Fundo',19),(20,'Riacho Fundo II',20),(21,'Samambaia',21),(22,'Santa Maria',22),(23,'São Sebastião',23),(24,'SCIA/Estrutural',24),(25,'SIA',25),(26,'Sobradinho',26),(27,'Sobradinho II',27),(28,'Sudoeste/Octogonal',28),(29,'Taguatinga',29),(30,'Varjão',30),(31,'Vicente Pires',31);
/*!40000 ALTER TABLE `tbbairro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbcidade`
--

DROP TABLE IF EXISTS `tbcidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbcidade` (
  `idCidade` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária',
  `nomeCidade` varchar(45) NOT NULL COMMENT 'Nome da cidade',
  `idEstado` int(11) NOT NULL COMMENT 'Chave estrangeira',
  PRIMARY KEY (`idCidade`),
  KEY `fk_tbCidade_tbEstado1_idx` (`idEstado`),
  CONSTRAINT `fk_tbCidade_tbEstado1` FOREIGN KEY (`idEstado`) REFERENCES `tbestado` (`idEstado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbcidade`
--

LOCK TABLES `tbcidade` WRITE;
/*!40000 ALTER TABLE `tbcidade` DISABLE KEYS */;
INSERT INTO `tbcidade` VALUES (1,'Águas Claras',7),(2,'Brasília',7),(3,'Brazlândia',7),(4,'Candangolândia',7),(5,'Ceilândia',7),(6,'Cruzeiro',7),(7,'Fercal',7),(8,'Gama',7),(9,'Guará',7),(10,'Itapoã',7),(11,'Jardim Botânico',7),(12,'Lago Norte',7),(13,'Lago Sul',7),(14,'Núcleo Bandeirante',7),(15,'Paranoá',7),(16,'Park Way',7),(17,'Planaltina',7),(18,'Recanto das Emas',7),(19,'Riacho Fundo',7),(20,'Riacho Fundo II',7),(21,'Samambaia',7),(22,'Santa Maria',7),(23,'São Sebastião',7),(24,'SCIA/Estrutural',7),(25,'SIA',7),(26,'Sobradinho',7),(27,'Sobradinho II',7),(28,'Sudoeste/Octogonal',7),(29,'Taguatinga',7),(30,'Varjão',7),(31,'Vicente Pires',7);
/*!40000 ALTER TABLE `tbcidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbestado`
--

DROP TABLE IF EXISTS `tbestado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbestado` (
  `idEstado` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária',
  `nomeEstado` varchar(45) NOT NULL COMMENT 'Nome do estado',
  `siglaEstado` varchar(2) NOT NULL COMMENT 'Sigla do estado',
  PRIMARY KEY (`idEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbestado`
--

LOCK TABLES `tbestado` WRITE;
/*!40000 ALTER TABLE `tbestado` DISABLE KEYS */;
INSERT INTO `tbestado` VALUES (1,'Acre','AC'),(2,'Alagoas','AL'),(3,'Amazonas','AM'),(4,'Amapá','AP'),(5,'Bahia','BA'),(6,'Ceará','CE'),(7,'Distrito Federal','DF'),(8,'Espírito Santo','ES'),(9,'Goiás','GO'),(10,'Maranhão','MA'),(11,'Minas Gerais','MG'),(12,'Mato Grosso do Sul','MS'),(13,'Mato Grosso','MT'),(14,'Pará','PA'),(15,'Paraíba','PB'),(16,'Pernambuco','PE'),(17,'Piauí','PI'),(18,'Paraná','PR'),(19,'Rio de Janeiro','RJ'),(20,'Rio Grande do Norte','RN'),(21,'Rondônia','RO'),(22,'Roraima','RR'),(23,'Rio Grande do Sul','RS'),(24,'Santa Catarina','SC'),(25,'Sergipe','SE'),(26,'São Paulo','SP'),(27,'Tocantins','TO');
/*!40000 ALTER TABLE `tbestado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblista`
--

DROP TABLE IF EXISTS `tblista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblista` (
  `idLista` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária',
  `nomeLista` varchar(45) NOT NULL COMMENT 'Nome da lista',
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idLista`),
  KEY `fk_tbLista_tbUsuario1_idx` (`idUsuario`),
  CONSTRAINT `fk_tbLista_tbUsuario1` FOREIGN KEY (`idUsuario`) REFERENCES `tbusuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='										';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblista`
--

LOCK TABLES `tblista` WRITE;
/*!40000 ALTER TABLE `tblista` DISABLE KEYS */;
INSERT INTO `tblista` VALUES (26,'Aniversário do Houston',1);
/*!40000 ALTER TABLE `tblista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblista_has_tbproduto`
--

DROP TABLE IF EXISTS `tblista_has_tbproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblista_has_tbproduto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idLista` int(11) NOT NULL,
  `idProduto` int(11) NOT NULL,
  `qtProduto` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbLista_has_tbProduto_tbProduto1_idx` (`idProduto`),
  KEY `fk_tbLista_has_tbProduto_tbLista1_idx` (`idLista`),
  CONSTRAINT `fk_tbLista_has_tbProduto_tbLista1` FOREIGN KEY (`idLista`) REFERENCES `tblista` (`idLista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbLista_has_tbProduto_tbProduto1` FOREIGN KEY (`idProduto`) REFERENCES `tbproduto` (`idProduto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblista_has_tbproduto`
--

LOCK TABLES `tblista_has_tbproduto` WRITE;
/*!40000 ALTER TABLE `tblista_has_tbproduto` DISABLE KEYS */;
INSERT INTO `tblista_has_tbproduto` VALUES (1,26,63,2),(2,26,64,100);
/*!40000 ALTER TABLE `tblista_has_tbproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmercado`
--

DROP TABLE IF EXISTS `tbmercado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbmercado` (
  `idMercado` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária',
  `nomeMercado` varchar(45) NOT NULL COMMENT 'Nome do mercado',
  `ruaMercado` varchar(45) DEFAULT NULL,
  `latitudeMercado` varchar(45) NOT NULL COMMENT 'Latitude do mercado',
  `longitudeMercado` varchar(45) NOT NULL COMMENT 'Longitude do mercado',
  `iconeMercado` varchar(100) DEFAULT NULL,
  `idUsuario` int(11) NOT NULL COMMENT 'Chave estrangeira',
  `idBairro` int(11) NOT NULL,
  PRIMARY KEY (`idMercado`),
  KEY `fk_tbMercado_tbUsuario1_idx` (`idUsuario`),
  KEY `fk_tbMercado_tbBairro1_idx` (`idBairro`),
  CONSTRAINT `fk_tbMercado_tbBairro1` FOREIGN KEY (`idBairro`) REFERENCES `tbbairro` (`idBairro`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbMercado_tbUsuario1` FOREIGN KEY (`idUsuario`) REFERENCES `tbusuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmercado`
--

LOCK TABLES `tbmercado` WRITE;
/*!40000 ALTER TABLE `tbmercado` DISABLE KEYS */;
INSERT INTO `tbmercado` VALUES (57,'Extra','Pistão Sul','-15.843371','-48.042317','logo_extra.png',1,29),(58,'Carrefour','Pistão Sul','-15.846209','-48.041456','logo_carrefour.png',1,29),(59,'Pão de Açúcar','Q 206 02','-15.841967','-48.022938','logo_pa.png',1,1),(60,'Super','Guará II QE 13','-15.825329','-47.979025','logo_supermaia.png',1,9);
/*!40000 ALTER TABLE `tbmercado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproduto`
--

DROP TABLE IF EXISTS `tbproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbproduto` (
  `idProduto` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária',
  `nomeProduto` varchar(50) NOT NULL COMMENT 'Nome do produto',
  `marcaProduto` varchar(50) DEFAULT NULL,
  `medidaProduto` float DEFAULT NULL,
  `descricaoProduto` varchar(50) DEFAULT NULL,
  `fotoProduto` varchar(50) DEFAULT NULL,
  `idUsuario` int(11) NOT NULL COMMENT 'Chave estrangeira',
  `idMedida` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProduto`),
  KEY `fk_tbProduto_tbUsuario1_idx` (`idUsuario`),
  KEY `fk_tbProduto_tbUnidadeMedida1_idx` (`idMedida`),
  CONSTRAINT `fk_tbProduto_tbUnidadeMedida1` FOREIGN KEY (`idMedida`) REFERENCES `tbunidademedida` (`idMedida`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbProduto_tbUsuario1` FOREIGN KEY (`idUsuario`) REFERENCES `tbusuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproduto`
--

LOCK TABLES `tbproduto` WRITE;
/*!40000 ALTER TABLE `tbproduto` DISABLE KEYS */;
INSERT INTO `tbproduto` VALUES (63,'Arroz','Tio João',5,'Tipo 1','ced5dacc.jpg',1,3),(64,'Cerveja','Skol',355,'Long neck','ea7a2d17.jpg',1,2);
/*!40000 ALTER TABLE `tbproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproduto_has_tbmercado`
--

DROP TABLE IF EXISTS `tbproduto_has_tbmercado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbproduto_has_tbmercado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `precoProduto` float NOT NULL,
  `idProduto` int(11) NOT NULL,
  `idMercado` int(11) NOT NULL,
  `idStatusProduto` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idProduto_Mercado_UNIQUE` (`id`),
  KEY `fk_tbProduto_has_tbMercado_tbMercado1_idx` (`idMercado`),
  KEY `fk_tbProduto_has_tbMercado_tbProduto_idx` (`idProduto`),
  KEY `fk_tbProduto_has_tbMercado_tbStatusProduto1_idx` (`idStatusProduto`),
  KEY `fk_tbProduto_has_tbMercado_tbUsuario1_idx` (`idUsuario`),
  CONSTRAINT `fk_tbProduto_has_tbMercado_tbMercado1` FOREIGN KEY (`idMercado`) REFERENCES `tbmercado` (`idMercado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbProduto_has_tbMercado_tbProduto` FOREIGN KEY (`idProduto`) REFERENCES `tbproduto` (`idProduto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbProduto_has_tbMercado_tbStatusProduto1` FOREIGN KEY (`idStatusProduto`) REFERENCES `tbstatusproduto` (`idStatus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tbProduto_has_tbMercado_tbUsuario1` FOREIGN KEY (`idUsuario`) REFERENCES `tbusuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproduto_has_tbmercado`
--

LOCK TABLES `tbproduto_has_tbmercado` WRITE;
/*!40000 ALTER TABLE `tbproduto_has_tbmercado` DISABLE KEYS */;
INSERT INTO `tbproduto_has_tbmercado` VALUES (1,12,63,58,2,1),(2,3,64,58,2,1);
/*!40000 ALTER TABLE `tbproduto_has_tbmercado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbstatusproduto`
--

DROP TABLE IF EXISTS `tbstatusproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbstatusproduto` (
  `idStatus` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária',
  `nomeStatus` varchar(45) NOT NULL COMMENT 'Nome do status do produto',
  PRIMARY KEY (`idStatus`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbstatusproduto`
--

LOCK TABLES `tbstatusproduto` WRITE;
/*!40000 ALTER TABLE `tbstatusproduto` DISABLE KEYS */;
INSERT INTO `tbstatusproduto` VALUES (1,'Normal'),(2,'Oferta');
/*!40000 ALTER TABLE `tbstatusproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtipousuario`
--

DROP TABLE IF EXISTS `tbtipousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtipousuario` (
  `idTipoUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nomeTipoUsuario` varchar(45) NOT NULL,
  PRIMARY KEY (`idTipoUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtipousuario`
--

LOCK TABLES `tbtipousuario` WRITE;
/*!40000 ALTER TABLE `tbtipousuario` DISABLE KEYS */;
INSERT INTO `tbtipousuario` VALUES (1,'Administrador'),(2,'Comum');
/*!40000 ALTER TABLE `tbtipousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbunidademedida`
--

DROP TABLE IF EXISTS `tbunidademedida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbunidademedida` (
  `idMedida` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primária',
  `nomeMedida` varchar(45) NOT NULL,
  `siglaMedida` varchar(45) NOT NULL,
  PRIMARY KEY (`idMedida`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='tbUnidadeMedidacol';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbunidademedida`
--

LOCK TABLES `tbunidademedida` WRITE;
/*!40000 ALTER TABLE `tbunidademedida` DISABLE KEYS */;
INSERT INTO `tbunidademedida` VALUES (1,'Litro','L'),(2,'Mililitro','ml'),(3,'Kilograma','kg'),(4,'Grama','g'),(6,'Unidade','u');
/*!40000 ALTER TABLE `tbunidademedida` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuario`
--

DROP TABLE IF EXISTS `tbusuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuario` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave primaria tbusuario',
  `nomeUsuario` varchar(50) NOT NULL COMMENT 'Nome do usuário',
  `cpfUsuario` varchar(11) NOT NULL COMMENT 'CPF do usuario',
  `emailUsuario` varchar(50) NOT NULL COMMENT 'Email do usuario',
  `senhaUsuario` varchar(6) NOT NULL COMMENT 'Senha do usuario',
  `idTipoUsuario` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `fk_tbUsuario_tbTipoUsuario1_idx` (`idTipoUsuario`),
  CONSTRAINT `fk_tbUsuario_tbTipoUsuario1` FOREIGN KEY (`idTipoUsuario`) REFERENCES `tbtipousuario` (`idTipoUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuario`
--

LOCK TABLES `tbusuario` WRITE;
/*!40000 ALTER TABLE `tbusuario` DISABLE KEYS */;
INSERT INTO `tbusuario` VALUES (1,'Fernando','12345678910','fgkm96@gmail.com','12345',1);
/*!40000 ALTER TABLE `tbusuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-11 18:01:48
