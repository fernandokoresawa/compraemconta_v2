-- -----------------------------------------------------
-- INSERTS compraemconta
-- -----------------------------------------------------

USE compraemconta;

-- -----------------------------------------------------
-- tbStatusProduto
-- -----------------------------------------------------
INSERT INTO tbStatusProduto
(tbStatusProduto.nomeStatus)
VALUES
('Normal'),
('Oferta');

-- -----------------------------------------------------
-- tbEstado
-- -----------------------------------------------------

INSERT INTO tbEstado
(tbEstado.nomeEstado, tbEstado.siglaEstado) 
VALUES
('Acre', 'AC'),
('Alagoas', 'AL'),
('Amazonas', 'AM'),
('Amapá', 'AP'),
('Bahia', 'BA'),
('Ceará', 'CE'),
('Distrito Federal', 'DF'),
('Espírito Santo', 'ES'),
('Goiás', 'GO'),
('Maranhão', 'MA'),
('Minas Gerais', 'MG'),
('Mato Grosso do Sul', 'MS'),
('Mato Grosso', 'MT'),
('Pará', 'PA'),
('Paraíba', 'PB'),
('Pernambuco', 'PE'),
('Piauí', 'PI'),
('Paraná', 'PR'),
('Rio de Janeiro', 'RJ'),
('Rio Grande do Norte', 'RN'),
('Rondônia', 'RO'),
('Roraima', 'RR'),
('Rio Grande do Sul', 'RS'),
('Santa Catarina', 'SC'),
('Sergipe', 'SE'),
('São Paulo', 'SP'),
('Tocantins', 'TO');


-- -----------------------------------------------------
-- tbCidade
-- -----------------------------------------------------
INSERT INTO tbCidade
(tbCidade.nomeCidade, tbCidade.idEstado)
VALUES
('Águas Claras', 7),
('Brasília', 7),
('Brazlândia', 7),
('Candangolândia', 7),
('Ceilândia', 7),
('Cruzeiro', 7),
('Fercal', 7),
('Gama', 7),
('Guará', 7),
('Itapoã', 7),
('Jardim Botânico', 7),
('Lago Norte', 7),
('Lago Sul', 7),
('Núcleo Bandeirante', 7),
('Paranoá', 7),
('Park Way', 7),
('Planaltina', 7),
('Recanto das Emas', 7),
('Riacho Fundo', 7),
('Riacho Fundo II', 7),
('Samambaia', 7),
('Santa Maria', 7),
('São Sebastião', 7),
('SCIA/Estrutural', 7),
('SIA', 7),
('Sobradinho', 7),
('Sobradinho II', 7),
('Sudoeste/Octogonal', 7),
('Taguatinga', 7),
('Varjão', 7),
('Vicente Pires', 7);

-- -----------------------------------------------------
-- tbBairro
-- -----------------------------------------------------
INSERT INTO tbBairro
(tbBairro.nomeBairro, tbBairro.idCidade)
VALUES
('Águas Claras', 1),
('Brasília', 2),
('Brazlândia', 3),
('Candangolândia', 4),
('Ceilândia', 5),
('Cruzeiro', 6),
('Fercal', 7),
('Gama', 8),
('Guará', 9),
('Itapoã', 10),
('Jardim Botânico', 11),
('Lago Norte', 12),
('Lago Sul', 13),
('Núcleo Bandeirante', 14),
('Paranoá', 15),
('Park Way', 16),
('Planaltina', 17),
('Recanto das Emas', 18),
('Riacho Fundo', 19),
('Riacho Fundo II', 20),
('Samambaia', 21),
('Santa Maria', 22),
('São Sebastião', 23),
('SCIA/Estrutural', 24),
('SIA', 25),
('Sobradinho', 26),
('Sobradinho II', 27),
('Sudoeste/Octogonal', 28),
('Taguatinga', 29),
('Varjão', 30),
('Vicente Pires', 31);


-- -----------------------------------------------------
-- tbTipoUsuario
-- -----------------------------------------------------
INSERT INTO tbTipoUsuario
(tbTipoUsuario.nomeTipoUsuario)
VALUES
('Administrador'),
('Comum');

-- -----------------------------------------------------
-- tbUsuario
-- -----------------------------------------------------
INSERT INTO tbUsuario
(tbUsuario.nomeUsuario, tbUsuario.cpfUsuario, tbUsuario.emailUsuario, tbUsuario.senhaUsuario, tbUsuario.idTipoUsuario)
VALUES
('Fernando', '12345678910', 'fgkm96@gmail.com', '12345', 1);


-- -----------------------------------------------------
-- tbUnidadeMedida
-- -----------------------------------------------------
INSERT INTO tbUnidadeMedida
(tbUnidadeMedida.nomeMedida, tbUnidadeMedida.siglaMedida)
VALUES
('Litro','L'),
('Mililitro', 'ml'),
('Kilograma', 'kg'),
('Grama', 'g');



-- -----------------------------------------------------
-- tbMercado
-- -----------------------------------------------------
insert into tbMercado 
(nomeMercado, ruaMercado, latitudeMercado, longitudeMercado, idUsuario, idBairro)
values
('Extra', 'CNB 10', '-102345', '-45489210', 1, 2),
('Pra voce', 'CNB 11', '-102345', '-45489210', 1, 2),
('Carrefour', 'EPNB', '-102345', '-45489210', 1, 2),
('BigBox', 'EPTG', '-102345', '-45489210', 1, 2),
('Atacadao', 'EPTG', '-102345', '-45489210', 1, 2),
('Atacadao', 'Via estadium', '-102345', '-45489210', 1, 2);



-- -----------------------------------------------------
-- tbProduto
-- -----------------------------------------------------
insert into tbproduto 
(nomeProduto, marcaProduto, medidaProduto, descricaoProduto, fotoProduto, idUsuario, idMedida) 
VALUES 
('Presunto', 'Perdigao', 500, 'Defumado', 'foto', 1, 3),
('Arroz', 'Tio Joao', 250, 'Risoto com Brocolis', 'foto', 1, 4),
('Oleo', 'Olinda', 500, 'Oleo composto de Soja e Azeite', 'foto', 1, 2),
('Macarrao Instantaneo', 'Nissin', 99, 'Lamen Hot Sabor Calabresa', 'foto', 1, 4),
('Sal', 'Cisne', 1, 'Sal grosso para churrasco', 'foto', 1, 3);



-- -----------------------------------------------------
-- tbProduto_has_tbMercado
-- -----------------------------------------------------
/insert into tbproduto_has_tbmercado
(precoProduto, idProduto, idMercado, idStatusProduto, idUsuario)
values
(5,19,10,1,1),
(6,19,11,1,1),
(7,19,12,1,1),
(4,19,13,1,1),
(10,19,14,1,1),
(13,19,15,1,1);
(5,7,10,1,1),
(6,7,11,1,1),
(7,7,12,1,1),
(4,7,13,1,1),
(10,7,14,1,1),
(13,7,15,1,1),
(5,8,10,1,1),
(6,8,11,1,1),
(7,8,12,1,1),
(4,8,13,1,1),
(10,8,14,1,1),
(13,8,15,1,1),
(5,14,10,1,1),
(6,14,11,1,1),
(7,14,12,1,1),
(4,14,13,1,1),
(10,14,14,1,1),
(13,14,15,1,1),
(5,20,10,1,1),
(6,20,11,1,1),
(7,20,12,1,1),
(4,20,13,1,1),
(10,20,14,1,1),
(13,20,15,1,1);
