<%--
    Document   : resultadoMercado
    Created on : Aug 26, 2016, 6:03:07 PM
    Author     : Fernando
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <style type="text/css">.odd { background-color: rgba(0,0,0,.1)}.even { background-color: rgba(0,0,0,0)}</style>
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGAÇÃO-->
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                <nav class="navbar wow slideInRight animated" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                                <span class="sr-only"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="#"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="navbar-collapse">
                            <ul class="nav">
                                <li title="">
                                    <a href="index.html">home</a>
                                </li>
                                <li title="" class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">produtos
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="cadastrarProduto.html">Cadastrar</a></li>
                                        <li><a href="pesquisarProduto.html">Pesquisar</a></li>
                                    </ul>
                                </li>
                                <li title="" class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">mercados
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="cadastrarMercado.html">Cadastrar</a></li>
                                        <li><a href="pesquisarMercado.html">Pesquisar</a></li>
                                    </ul>
                                </li>
                                <li title="" class="dropdown">
                                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">listas
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="cadastrarLista.html">Cadastrar</a></li>
                                        <li><a href="pesquisarLista.html">Pesquisar</a></li>
                                    </ul>
                                </li>
                                <li title="">
                                    <a href="login.html">login</a>
                                </li>
                                <li title="">
                                    <a href="mapa.html">mapa</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <!--<img class="img-responsive img-header wow slideInDown animated" src="" />-->
        <!--CABECALHO-->
        <div class="page-header wow slideInLeft animated">
            <h1>CompraEmConta</h1>
        </div>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <!--OFERTAS-->
            <section id="" class="mysection">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                            <blockquote class="wow fadeInUp animated blocktitulo">Resultado</blockquote>
                            <table class="tabela-result" border="1">
                                <thead style="background-color: #fcc;">
                                    <td>
                                        Mercado
                                    </td>
                                    <td>
                                        Endereço
                                    </td>
                                    <td>
                                        Estado
                                    </td>
                                    <td>
                                        Cidade
                                    </td>
                                    <td>
                                        Latitude
                                    </td>
                                    <td>
                                        Longitude
                                    </td>
                                    <td>
                                        Excluir
                                    </td>
                                    <td>
                                        Editar
                                    </td>
                                </thead>
                                <c:forEach items="${Mercados}" var="resultado" varStatus="row">
                                    <c:choose>
                                        <c:when test="${row.count % 2 == 0}">
                                            <c:set var="estiloLinha" value="odd"/>
                                        </c:when>
                                        <c:otherwise>
                                            <c:set var="estiloLinha" value="even"/>
                                        </c:otherwise>
                                    </c:choose>
                                    <tr class="${estiloLinha}">
                                        <td>
                                            <b>${resultado.nomeMercado}</b>
                                        </td>
                                        <td>
                                            ${resultado.enderecoMercado}
                                        </td>
                                        <td>
                                            ${resultado.estadoMercado}
                                        </td>
                                        <td>
                                            ${resultado.cidadeMercado}
                                        </td>
                                        <td>
                                            ${resultado.latitudeMercado}
                                        </td>
                                        <td>
                                            ${resultado.longitudeMercado}
                                        </td>
                                        <td>
                                            <a href="javascript:func()" onclick="return confirmaExclusao('${resultado.idMercado}')">
                                                <button>
                                                    <img src="./imagens/rub24.png" title="Excluir" />
                                                </button>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="javascript:func()" onclick="return confirmaEdicao('${resultado.idMercado}')">
                                                <button>
                                                    <img src="./imagens/pencil24.png" title="Editar"/>
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</body>
</html>
