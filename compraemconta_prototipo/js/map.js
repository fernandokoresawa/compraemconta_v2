// Váriáveis necessárias
var map;
var marker;

function initialize() {
   var mapOptions = {
      center: new google.maps.LatLng(-15.830308,-47.889404),
      zoom: 13,
      mapTypeId: 'roadmap'
   };

   map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

   // Evento que detecta o click no mapa para criar o marcador
   google.maps.event.addListener(map, "click", function(event) {

      var lat = event.latLng.lat().toFixed(6);
      var lng = event.latLng.lng().toFixed(6);

      // cria o marcador e passa latitude e longitude
      createMarker(lat, lng);

      // atualiza inputs no form
      getCoords(lat, lng);

   });


}
google.maps.event.addDomListener(window, 'load', initialize);

// Função que cria o marcador
function createMarker(lat, lng) {

   // A intenção é criar um único marcador, por isso
   // verificamos se já existe um marcador no mapa.
   // Assim cada vez que é feito um click no mapa
   // o anterior marcador é removido e é criado outro novo.

   // Se a variável marker contém algum valor
   if (marker) {
      // remover esse marcador do mapa
      marker.setMap(null);
      // remover qualquer valor da variável marker
      marker = "";
   }

   // definir a variável marker com os novos valores
   marker = new google.maps.Marker({

      // Define a posição do marcador através dos valores lat e lng
      // que foram definidos através do click no mapa
      position: new google.maps.LatLng(lat, lng),

      // Esta opção permite que o marcador possa ser arrastado
      // para um posicionamento com maior precisão.
      draggable: true,

      map: map
   });


   // Evento que detecta o arrastar do marcador para
   // redefinir as coordenadas lat e lng e
   // actualiza os valores das caixas de texto no topo da página
   google.maps.event.addListener(marker, 'dragend', function() {

      // Actualiza as coordenadas de posição do marcador no mapa
      marker.position = marker.getPosition();

      // Redefine as variáveis lat e lng para actualizar
      // os valores das caixas de texto no topo
      var lat = marker.position.lat().toFixed(6);
      var lng = marker.position.lng().toFixed(6);

      // Chamada da função que actualiza os valores das caixas de texto
      getCoords(lat, lng);

   });
}

// Função que actualiza as caixas de texto no topo da página
function getCoords(lat, lng) {

   // Referência ao elemento HTML (input) com o id 'lat'
   var coords_lat = document.getElementById('lat');

   // Actualiza o valor do input 'lat'
   coords_lat.value = lat;

   // Referência ao elemento HTML (input) com o id 'lng'
   var coords_lng = document.getElementById('lng');

   // Actualiza o valor do input 'lng'
   coords_lng.value = lng;
}