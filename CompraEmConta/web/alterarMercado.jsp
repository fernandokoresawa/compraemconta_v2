<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.min.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA8yNtnyJdikIzrPq6ZSPfDYO8jph5lOGs"></script>
    <script type="text/javascript" src="js/map.js"></script>
    <script type="text/javascript">
        function ValidaCampoVazio(){
            if (document.getElementById("nomeMercado").value == ""){
                alert("Informe o nome do Mercado");
                document.form.nomeMercado.focus();
                return false;
            }

            if (document.getElementById("ruaMercado").value == ""){
                alert("Informe a rua do mercado");
                document.form.enderecoMercado.focus();
                return false;
            }            

            if(document.getElementById("lat").value == ""){
                alert("Informe a latitude do mercado");
                document.form.lat.focus();
                return false;
            }

            if(document.getElementById("lng").value == ""){
                alert("Informe a longitude do mercado");
                document.form.lng.focus();
                return false;
            }

        }
    </script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGAÇÃO-->
        <%@include file="header.jsp" %>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <section id="" class="mysection">
                <div class="row row-mercado">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                        <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                            <blockquote class="wow fadeInUp animated blocktitulo">Alterar Mercado</blockquote>
                            <div class="form wow slideInRight animated">
                                <form id="form" name="form" class="formulario" action="AlterarMercado" method="POST">
                                    <c:forEach items="${mercados}" var="mercados">
                                        <input type="hidden" id="idMercado" name="idMercado" value="${mercados.idMercado}" />
                                        <input class="input" type="text" id="nomeMercado" name="nomeMercado" placeholder="Nome do Mercado" value="${mercados.nomeMercado}" /><br/>
                                    <select class="input" id="selectEstado" name="selectEstado">
                                        <option value="${mercados.getEstado().getIdEstado()}"> ${mercados.getEstado().getSiglaEstado()} </option>
                                        <option></option>
                                        <c:forEach items="${estados}" var="estados">
                                            <option value="${estados.idEstado}">
                                                ${estados.siglaEstado}
                                            </option>
                                        </c:forEach>
                                    </select><br/>
                                    <select class="input" id="selectCidade" name="selectCidade">
                                        <option value="${mercados.getCidade().getIdCidade()}"> ${mercados.getCidade().getNomeCidade()} </option>
                                        <option></option>
                                        <c:forEach items="${cidades}" var="cidades">
                                            <option value="${cidades.idCidade}">
                                                ${cidades.nomeCidade}
                                            </option>
                                        </c:forEach>
                                    </select><br/>
                                    <select class="input" id="selectBairro" name="selectBairro">
                                        <option value="${mercados.getBairro().getIdBairro()}"> ${mercados.getBairro().getNomeBairro()} </option>
                                        <option></option>
                                        <c:forEach items="${bairros}" var="bairros">
                                            <option value="${bairros.idBairro}">
                                                ${bairros.nomeBairro}
                                            </option>
                                        </c:forEach>
                                    </select><br/>
                                    <input class="input" type="text" id="ruaMercado" name="ruaMercado" placeholder="Rua" value="${mercados.ruaMercado}"/><br/>
                                    <input class="input" type="text" id="lat" name="latitudeMercado" placeholder="Latitude" value="${mercados.latitudeMercado}" /><br/>
                                    <input class="input" type="text" id="lng" name="longitudeMercado" placeholder="Longitude" value="${mercados.longitudeMercado}" /><br/>
                                    <button onclick="return ValidaCampoVazio()" class="botaosubmit" type="submit">Alterar Mercado</button>
                                    </c:forEach>
                                </form>
                            </div>
                        </div>
                        <div id="map-canvas" class="map-canvas col-lg-6" title="MAPA"></div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</body>
</html>
