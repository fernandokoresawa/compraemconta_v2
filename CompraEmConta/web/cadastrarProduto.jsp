<%-- 
    Document   : cadastrarProduto
    Created on : Oct 18, 2016, 3:00:41 PM
    Author     : Fernando
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.min.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
    <script type="text/javascript">
        function ValidaCampoVazio(){
            if (document.getElementById("nomeProduto").value == ""){
                alert("Informe o nome do Produto");
                document.form.nomeProduto.focus();
                return false;
            }

            if (document.getElementById("marcaProduto").value == ""){
                alert("Informe a marca do produto");
                document.form.marcaProduto.focus();
                return false;
            }

            if (document.getElementById("medidaProduto").value == ""){
                alert("Informe a medida do produto");
                document.form.medidaProduto.focus();
                return false;
            }

            if (document.getElementById("selectMedida").selectedIndex == ""){
                alert("Escolha a unidade de medida");
                document.getElementById("selectMedida").focus();
                return false;
            }

            if (document.getElementById("descricaoProduto").value == ""){
                alert("Informe descricao do produto");
                document.form.descricaoProduto.focus();
                return false;
            }

            if (document.getElementById("precoProduto").value == ""){
                alert("Informe o preco do produto");
                document.form.precoProduto.focus();
                return false;
            }

            if (document.getElementById("selectStatus").selectedIndex == ""){
                alert("Escolha o status do produto");
                document.getElementById("selectStatus").focus();
                return false;
            }

            if (document.getElementById("selectMercado").selectedIndex == ""){
                alert("Escolha o mercado");
                document.getElementById("selectMercado").focus();
                return false;
            }

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeTo(200, 0).slideUp(150, function(){
                    $(this).remove();
                });
            }, 3000);
        });
    </script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGA��O-->
        <%@include file="header.jsp" %>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <section id="" class="mysection">
                <div class="row">
                    <c:if test="${msgSucesso != null}">
                        <div class="alert col-sm-12 col-md-12 col-lg-12 col-xs-12" role="alert">
                            <strong>${msgSucesso}</strong>
                        </div>
                    </c:if>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                        <blockquote class="wow fadeInUp animated blocktitulo">Cadastrar Produto</blockquote>
                        <div class="form wow slideInRight animated">
                            <form id="form" name="form" class="formulario" action="IncluirProduto" method="POST">
                                <input class="input" type="text" id="nomeProduto" name="nomeProduto" placeholder="Nome do produto" /><br/>
                                <input class="input" type="text" id="marcaProduto" name="marcaProduto" placeholder="marca do produto" /><br/>
                                <div>
                                <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                                <input class="input" type="number" step="any" id="medidaProduto" name="medidaProduto" placeholder="600" min="1" />
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                                <select class="input" id="selectMedida" name="selectMedida">
                                    <option value=""> - Unidade medida - </option>
                                    <c:forEach items="${medidas}" var="medidas">
                                        <option value="${medidas.idMedida}">
                                            ${medidas.siglaMedida}
                                        </option>
                                    </c:forEach>
                                </select>
                                </div>
                                </div>
                                <input class="input" type="text" id="descricaoProduto" name="descricaoProduto" placeholder="descricao" /><br/>
                                <input class="input" type="number" step="any" id="precoProduto" name="precoProduto" placeholder="precoProduto" min="1" /><br/>
                                <!--<input class="input" type="file" id="fotoProduto" name="file" placeholder="foto" /><br/>-->
                                <input class="input" type="text" id="fotoProduto" name="fotoProduto" placeholder="foto" /><br/>
                                <select class="input" id="selectStatus" name="selectStatus">
                                    <option value=""> - Status - </option>
                                    <c:forEach items="${status}" var="status">
                                        <option value="${status.idStatusProduto}">
                                            ${status.nomeStatusProduto}
                                        </option>
                                    </c:forEach>
                                </select><br/>
                                <select class="input" id="selectMercado" name="selectMercado">
                                    <option value=""> - Mercado - </option>
                                    <c:forEach items="${mercados}" var="mercados">
                                        <option value="${mercados.idMercado}">
                                            ${mercados.nomeMercado} - ${mercados.ruaMercado}
                                        </option>
                                    </c:forEach>
                                </select><br/>
                                <button onclick="return ValidaCampoVazio()" class="botaosubmit" type="submit">Cadastrar Produto</button>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </body>
</html>
