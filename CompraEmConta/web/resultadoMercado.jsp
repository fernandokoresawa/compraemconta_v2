<%-- 
    Document   : resultadoMercado
    Created on : Oct 11, 2016, 1:21:15 AM
    Author     : Fernando
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
    <style type="text/css">.odd { background-color: rgba(0,0,0,.1)}.even { background-color: rgba(0,0,0,0)}</style>
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
    <script language="JavaScript" type="text/javascript">
        function confirmaExclusao(idMercado){
            var resposta = confirm("Deseja excluir este mercado?");
            if(resposta ==  true){
                location.href = "http://localhost:8080/CompraEmConta/ExcluirMercado?idMercado="+idMercado;
            }
            return false
        }

        function editar(idMercado){
            location.href = "http://localhost:8080/CompraEmConta/AlterarMercadoForm?idMercado="+idMercado;
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeTo(200, 0).slideUp(150, function(){
                    $(this).remove();
                });
            }, 3000);
        });
    </script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGA��O-->
        <%@include file="header.jsp" %>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <section id="" class="mysection">
                <div class="row">
                    <c:if test="${msgSucesso != null}">
                        <div class="alert col-sm-12 col-md-12 col-lg-12 col-xs-12" role="alert">
                            <strong>${msgSucesso}</strong>
                        </div>
                    </c:if>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                            <blockquote class="wow fadeInUp animated blocktitulo">Resultado da pesquisa</blockquote>
                            <div class="form wow slideInRight animated">
                                <table class="tabela-result" border="1">
                                    <thead>
                                        <td>Mercado</td>
                                        <td>Endere�o</td>
                                        <td>Bairro</td>
                                        <td>Cidade</td>
                                        <td>Estado</td>
                                        <td>Mapa</td>
                                        <td>Editar</td>
                                        <td>Excluir</td>
                                    </thead>
                                    <c:forEach items="${mercados}" var="mercados" varStatus="row">
                                        <c:choose>
                                            <c:when test="${row.count % 2 == 0}">
                                                <c:set var="estiloLinha" value="odd"/>
                                            </c:when>
                                            <c:otherwise>
                                                <c:set var="estiloLinha" value="even"/>
                                            </c:otherwise>
                                        </c:choose>
                                        <tr class="${estiloLinha}">
                                            <td>${mercados.nomeMercado}</td>
                                            <td>${mercados.ruaMercado}</td>
                                            <td>${mercados.getBairro().getNomeBairro()}</td>
                                            <td>${mercados.getCidade().getNomeCidade()}</td>
                                            <td>${mercados.getEstado().getNomeEstado()}</td>
                                            <td>MAPA</td>
                                            <td style="border: 1px solid #000; padding: 5px;">
                                                <a href="javascript:func()" onclick="return editar('${mercados.idMercado}')">
                                                    <button>
                                                        <img src="./imagens/pencil24.png" title="Editar"/>
                                                    </button>
                                                </a>
                                            </td>
                                            <td style="border: 1px solid #000; padding: 5px;">
                                                <a href="javascript:func()" onclick="return confirmaExclusao('${mercados.idMercado}')">
                                                    <button>
                                                        <img src="./imagens/rub24.png" title="Excluir" />
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</body>
</html>
