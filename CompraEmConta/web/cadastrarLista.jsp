
<%-- 
    Document   : cadastrarLista
    Created on : Oct 10, 2016, 11:20:46 PM
    Author     : Fernando
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.forms.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
    <!-- AUTOCOMPLETE -->
    <script>
    $( function() {
    var availableTags = [
      <%
        String produtos = "";
        for(String nomes : (List<String>) request.getAttribute("produtos")){
          produtos += "\"" + nomes + "\",";
        }
        out.print(produtos.substring(0, produtos.lastIndexOf(",")));
      %>
    ];
    $( "#nomeProduto" ).autocomplete({
      source: availableTags
    });
    } );
    </script>
    <!-- INPUT DINAMICO -->
    <script>
    $(function () {
        var scntDiv = $('#dynamicDiv');
        var id = 0;

        $(document).on('click', '#addInput', function () {
            id++;
            $(
            '<div> '+
                '<input class="input" id="nomeProduto'+id+'" type="text" name="nomeProduto" placeholder="Produto" /> '+
                '<input class="input input-qtproduto" type="number" name="qtProduto" value="1" min="1" style="width: 7%" /> '+
                '<a title="Excluir produto" class="btn btn-danger" href="javascript:void(0)" id="remInput"> '+
                    '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> '+
                '</a> '+
            '</div>'
            ).appendTo(scntDiv);
    
    var availableTags = [
      <%
        produtos = "";
        for(String nomes : (List<String>) request.getAttribute("produtos")){
          produtos += "\"" + nomes + "\",";
        }
        out.print(produtos.substring(0, produtos.lastIndexOf(",")));
      %>
    ];
    
    //console.log('id:'+id);
    
    $( "#nomeProduto"+id).autocomplete({
      source: availableTags
    });
            return false;
        });

        $(document).on('click', '#remInput', function () {
              $(this).parents('#dynamicDiv div').remove();
            return false;
        });
    });
    </script>
    <!--SHOW MODAL-->
    <script>

        function janelaSelecionaProduto(){

            $('#modalSelecionaProduto').modal('show');
        }

    </script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGAÇÃO-->
        <%@include file="header.jsp" %>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <section id="" class="mysection">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                            <blockquote class="wow fadeInUp animated blocktitulo">Cadastrar Lista</blockquote>
                            <div class="form wow slideInRight animated">
                                <form class="formulario" action="IncluirLista_Produto" method="POST">
                                    <div>
                                        <input class="input" type="text" name="nomeLista" placeholder="Nome da Lista" />
                                        <div id="dynamicDiv">
                                            <div>
                                                <a title="Selecionar produto" href="javascript:janelaSelecionaProduto();">
                                                    <img src="imagens/search-white.png">
                                                </a>
                                                <input class="input" id="nomeProduto" type="text" name="nomeProduto" placeholder="Produto" />
                                                <input class="input input-qtproduto" type="number" value="1" name="qtProduto" placeholder="1" min="1" style="width: 7%" />
                                                <span class="btn btn-hide"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-add">
                                        <a title="Adicionar produto" class="btn btn-primary" href="javascript:void(0)" id="addInput">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        </a>
                                    </div>
                                    <button class="botaosubmit" type="submit">Cadastrar Lista</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!--MODAL-->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalSelecionaProduto" data-target="#modalSelecionaProduto" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
                    <h4 class="modal-title">Selecionar Produto</h4>
                </div>
                <div class="modal-body">
                    <form role="form" method="post" action="" id="">
                        <div class="">
                            <label for="">Produto</label>
                            <input type="text" class="nomeProdutoM" id="nomeProdutoM" name="nomeProdutoM">
                        </div>
                        <input type="hidden" name="idProd" id="idProd" value="" />
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="button" class="btn btn-primary" type="submit">Incluir</button>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</body>
</html>
