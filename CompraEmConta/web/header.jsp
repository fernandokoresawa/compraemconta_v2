<%-- 
    Document   : header
    Created on : Oct 10, 2016, 11:23:24 PM
    Author     : Fernando
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<body>
    <!--MENU/NAVEGAÃ‡ÃƒO-->
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
            <nav class="navbar wow slideInRight animated" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                            <span class="sr-only"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand hidden-sm hidden-md hidden-lg" href="#"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav">
                            <li title="">
                                <a href="Home">home</a>
                            </li>
                            <li title="" class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">produtos
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="CadastrarProdutoForm">Cadastrar</a></li>
                                    <li><a href="PesquisarProdutoForm">Pesquisar</a></li>
                                </ul>
                            </li>
                            <li title="" class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">mercados
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="CadastrarMercadoForm">Cadastrar</a></li>
                                    <li><a href="PesquisarMercadoForm">Pesquisar</a></li>
                                </ul>
                            </li>
                            <li title="" class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">listas
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="CadastrarListaForm">Cadastrar</a></li>
                                    <li><a href="PesquisarListaForm">Pesquisar</a></li>
                                </ul>
                            </li>
                            <li title="" class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login
                                </a>
                                <ul class="dropdown-menu">
                                    <li title=""><a href="login.jsp">logar</a></li>
                                    <li><a href="cadastrarUsuario.jsp">Novo usuário</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <!--CABECALHO-->
    <div class="page-header wow slideInLeft animated">
        <h1>CompraEmConta</h1>
    </div>
</body>
</html>
