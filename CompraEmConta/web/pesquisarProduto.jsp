<%@page import="java.util.List"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
    <!-- AUTOCOMPLETE -->
    <script>
    $( function() {
    var availableTags = [
      <%
        String produtos = "";
        for(String nomes : (List<String>) request.getAttribute("produtos")){
          produtos += "\"" + nomes + "\",";
        }
        out.print(produtos.substring(0, produtos.lastIndexOf(",")));
      %>
    ];
    $( "#nomeProduto" ).autocomplete({
      source: availableTags
    });
    } );
    </script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGA��O-->
        <%@include file="header.jsp" %>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <section id="" class="mysection">
                <div class="row">
                    <c:if test="${msgSucesso != null}">
                        <div class="alert col-sm-12 col-md-12 col-lg-12 col-xs-12" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <strong>${msgSucesso}</strong>
                        </div>
                    </c:if>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                            <blockquote class="wow fadeInUp animated blocktitulo">Pesquisar Produto</blockquote>
                            <div class="form wow slideInRight animated">
                                <form class="formulario" action="PesquisarProduto" method="POST">
                                    <input class="input" id="nomeProduto" type="text" name="nomeProduto" placeholder="Produto" /><br/>
                                    <button class="botaosubmit" type="submit">Pesquisar Produto</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</body>
</html>