<%-- 
    Document   : resultadoProdutos
    Created on : Oct 11, 2016, 1:21:15 AM
    Author     : Fernando
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGA��O-->
        <%@include file="header.jsp" %>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <section id="" class="mysection">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                            <!--<blockquote class="wow fadeInUp animated blocktitulo"></blockquote>-->
                            <c:forEach items="${produtos}" var="produtos">
                            <div class="col-sm-6 col-md-3 col-lg-3 col-xs-12 oferta wow slideInRight animated">
                                <h3>${produtos.getProduto().getNomeProduto()}<br/><small>R$ ${produtos.precoProduto}</small></h3>
                                <h5>${produtos.getStatusProduto().getNomeStatusProduto()}</h5>
                                <img class="img-responsive img-prod" src="imagens/arroz-tio-joao.png" />
                                <h4 class="mercado">
                                    ${produtos.getMercado().getNomeMercado()}<br/>
                                    <small>${produtos.getMercado().getRuaMercado()}</small><br/>
                                    <small>${produtos.getBairro().getNomeBairro()}</small>
                                </h4>
                            </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</body>
</html>
