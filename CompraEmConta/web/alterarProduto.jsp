<%-- 
    Document   : cadastrarProduto
    Created on : Oct 18, 2016, 3:00:41 PM
    Author     : Fernando
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.min.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
    <script type="text/javascript">
        function ValidaCampoVazio(){
            if (document.getElementById("nomeProduto").value == ""){
                alert("Informe o nome do Produto");
                document.form.nomeProduto.focus();
                return false;
            }

            if (document.getElementById("marcaProduto").value == ""){
                alert("Informe a marca do produto");
                document.form.marcaProduto.focus();
                return false;
            }

            if (document.getElementById("medidaProduto").value == ""){
                alert("Informe a medida do produto");
                document.form.medidaProduto.focus();
                return false;
            }

            if (document.getElementById("selectMedida").selectedIndex == ""){
                alert("Escolha a unidade de medida");
                document.getElementById("selectMedida").focus();
                return false;
            }

            if (document.getElementById("descricaoProduto").value == ""){
                alert("Informe descricao do produto");
                document.form.descricaoProduto.focus();
                return false;
            }

            if (document.getElementById("precoProduto").value == ""){
                alert("Informe o preco do produto");
                document.form.precoProduto.focus();
                return false;
            }

            if (document.getElementById("selectStatus").selectedIndex == ""){
                alert("Escolha o status do produto");
                document.getElementById("selectStatus").focus();
                return false;
            }

            if (document.getElementById("selectMercado").selectedIndex == ""){
                alert("Escolha o mercado");
                document.getElementById("selectMercado").focus();
                return false;
            }

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
                    $(this).remove();
                });
            }, 2500);
        });
    </script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGAÇÃO-->
        <%@include file="header.jsp" %>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <section id="" class="mysection">
                <div class="row">
                    <c:if test="${msgSucesso != null}">
                        <div class="msg-alert alert" role="alert">
                            <span>${msgSucesso}</span>
                        </div>
                    </c:if>
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                        <blockquote class="wow fadeInUp animated blocktitulo">Alterar Produto</blockquote>
                        <div class="form wow slideInRight animated">
                            <form id="form" name="form" class="formulario" action="AlterarProduto" method="POST">
                                <c:forEach items="${produtos}" var="produtos">
                                    <input type="hidden" value="${produtos.getProduto().getIdProduto()}" name="idProduto" />
                                    <input type="hidden" value="${produtos.getMercado().getIdMercado()}" name="idMercado" />
                                    <input class="input" type="text" id="nomeProduto" name="nomeProduto" value="${produtos.getProduto().getNomeProduto()}" placeholder="Nome do produto" /><br/>
                                    <input class="input" type="text" id="marcaProduto" name="marcaProduto" value="${produtos.getProduto().getMarcaProduto()}" placeholder="marca do produto" /><br/>
                                    <div class="rowm col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                    <div class="input-float input-float-l col-sm-6 col-md-6 col-lg-6 col-xs-6">
                                    <input class="input" type="number" step="any" id="medidaProduto" name="medidaProduto" value="${produtos.getProduto().getMedidaProduto()}" min="1" />
                                    </div>
                                    <div class="input-float input-float-r col-sm-6 col-md-6 col-lg-6 col-xs-6">
                                    <select class="input" id="selectMedida" name="selectMedida">
                                        <option value="${produtos.getUnidade().getIdMedida()}"> ${produtos.getUnidade().getSiglaMedida()} </option>
                                        <option></option>
                                        <c:forEach items="${medidas}" var="medidas">
                                            <option value="${medidas.idMedida}">
                                                ${medidas.siglaMedida}
                                            </option>
                                        </c:forEach>
                                    </select>
                                    </div>
                                    </div>
                                    <input class="input" type="text" id="descricaoProduto" name="descricaoProduto"value="${produtos.getProduto().getDescricaoProduto()}" placeholder="descricao" /><br/>
                                    <input class="input" type="number" step="any" id="precoProduto" name="precoProduto" value="${produtos.precoProduto}" placeholder="precoProduto" min="1" /><br/>
                                    <input class="input" type="text" id="fotoProduto" name="fotoProduto" value="${produtos.getProduto().getFotoProduto()}" placeholder="foto" /><br/>
                                    <select class="input" id="selectStatus" name="selectStatus">
                                        <option value="${produtos.getStatusProduto().getIdStatusProduto()}"> ${produtos.getStatusProduto().getNomeStatusProduto()} </option>
                                        <option></option>
                                        <c:forEach items="${status}" var="status">
                                            <option value="${status.idStatusProduto}">
                                                ${status.nomeStatusProduto}
                                            </option>
                                        </c:forEach>
                                    </select><br/>
                                    <select class="input" id="selectMercado" name="selectMercado">
                                        <option value="${produtos.getMercado().getIdMercado()}"> ${produtos.getMercado().getNomeMercado()} </option>
                                        <option></option>
                                        <c:forEach items="${mercados}" var="mercados">
                                            <option value="${mercados.idMercado}">
                                                ${mercados.nomeMercado}
                                            </option>
                                        </c:forEach>
                                    </select><br/>
                                    <button onclick="return ValidaCampoVazio()" class="botaosubmit" type="submit">Atualizar Produto</button>
                            </c:forEach>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    </body>
</html>

