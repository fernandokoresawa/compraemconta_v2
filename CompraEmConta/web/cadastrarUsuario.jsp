<%-- 
    Document   : cadastrarLista
    Created on : Oct 10, 2016, 11:20:46 PM
    Author     : Fernando
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="desktop landscape" lang="pt-br">
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <title>CompraEmConta</title>
    <link rel="shortcut icon" href="imagens/shopping-cart.png" />
    <!--CSS-->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="css/style.css" media="screen" />
    <link type="text/css" rel="stylesheet" href="css/bootstrap-theme.css" />
    <link type="text/css" rel="stylesheet" href="css/swiper.min.css" />
    <link type="text/css" rel="stylesheet" href="css/jquery-ui.css" />
    <!--JQUERY-->
    <script type="text/javascript" src="js/ajax.jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="js/jquery-ui.js"></script>
    <!--ANIMACAO-->
    <link type="text/css" rel="stylesheet" href="css/animate.css" />
    <script>new WOW().init();</script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <!--JAVASCRIPT-->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/funcoes.js"></script>
</head>
<body style="background-image: url(imagens/mercado.jpg);">
    <div class="container content container-fluid interface">
        <!--MENU/NAVEGAÇÃO-->
        <%@include file="header.jsp" %>
        <!--CORPO DA PAGINA-->
        <div class="content">
            <section id="" class="mysection">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12 jumbotron jumbotron-fluid">
                            <blockquote class="wow fadeInUp animated blocktitulo">Cadastrar novo usuário</blockquote>
                            <div class="form wow slideInRight animated">
                                <form class="formulario" action="CadastrarUsuario" method="POST">
                                    <input type="text" id="nomeUsuario" name="nomeUsuario" placeholder="Digite seu nome" class="input" /><br/>
                                    <input type="text" id="cpfUsuario" name="cpfUsuario" placeholder="Digite seu CPF" class="input" /><br/>
                                    <input type="text" id="emailUsuario" name="emailUsuario" placeholder="Digite seu e-mail" class="input" /><br/>
                                    <input type="text" id="senhaUsuario" name="senhaUsuario" placeholder="Digite sua senha" class="input" /><br/>
                                    <input type="text" id="senhaUsuarioConf" name="senhaUsuarioConf" placeholder="Confirme a senha" class="input" /><br/>
                                    <button class="botaosubmit" type="submit">Cadastrar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</body>
</html>
