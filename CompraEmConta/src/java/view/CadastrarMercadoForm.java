package view;

import Model.Bairro;
import Model.Cidade;
import Model.Estado;
import Model.Conexao;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
 *
 * @author Fernando
 */
public class CadastrarMercadoForm extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            
            Conexao conexao = new Conexao();
            
            conexao.getConnection();
            
            // montar combo cidade
            java.sql.Statement stat1 = conexao.getConnection().createStatement();
            
            String query1 = "SELECT idCidade, nomeCidade FROM tbCidade";
            
            ResultSet result1 = stat1.executeQuery(query1);
            
            ArrayList<Cidade> cidades = new ArrayList<>();
            
            while(result1.next()){
                Cidade cidade = new Cidade();
                cidade.setIdCidade(result1.getInt(1));
                cidade.setNomeCidade(result1.getString(2));                
                cidades.add(cidade);
            }
            
            request.setAttribute("cidades", cidades);
            
            
            // montar combo bairro
            java.sql.Statement stat2 = conexao.getConnection().createStatement();
            
            String query2 = "SELECT idBairro, nomeBairro FROM tbBairro";
            
            ResultSet result2 = stat2.executeQuery(query2);
            
            ArrayList<Bairro> bairros = new ArrayList<>();
            
            while(result2.next()){
                Bairro b = new Bairro();
                b.setIdBairro(result2.getInt(1));
                b.setNomeBairro(result2.getString(2));
                bairros.add(b);
            }
            
            request.setAttribute("bairros", bairros);
            
            
            // montar combo estado
            java.sql.Statement stat3 = conexao.getConnection().createStatement();
                        
            String select1 = "SELECT idEstado, siglaEstado FROM tbEstado";
            
            ResultSet result3 = stat3.executeQuery(select1);
            
            ArrayList<Estado> estados = new ArrayList<>();
            
            while(result3.next()){
                Estado estado = new Estado();
                estado.setIdEstado(result3.getInt(1));
                estado.setSiglaEstado(result3.getString(2));
                estados.add(estado);
            }
            
            request.setAttribute("estados", estados);
            
            request.getRequestDispatcher("cadastrarMercado.jsp").forward(request, response);
                        
        }
        catch(Exception e){
            e.printStackTrace();
        } 
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
