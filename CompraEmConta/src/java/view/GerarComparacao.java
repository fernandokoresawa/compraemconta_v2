package view;

import Model.Conexao;
import Model.Mercado;
import Model.Produto_Preco;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class GerarComparacao extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
        
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            int idLista = Integer.parseInt(request.getParameter("idLista"));
            
            String selectMercado = 
                "SELECT m.idMercado FROM tbmercado m ";
            
            ResultSet resultM = stat.executeQuery(selectMercado);
            
            ArrayList<Mercado> mercados = new ArrayList<>();
            
            while(resultM.next()){
                Mercado m = new Mercado();
                m.setIdMercado(resultM.getInt(1));
                mercados.add(m);
            }
            
            //System.out.println("########################## "+mercados);
            
            String query = "SELECT IFNULL(produto, 'Total') AS 'Nome',";
            
            // loop mercados
            for(int i = 0; i < mercados.size(); i++){
                query += "sub.mercado"+(i+1)+" ";
                if(i != mercados.size() - 1){
                    query += ", ";
                }
            }
            
            query += "\nFROM (SELECT CONCAT(p.nomeProduto, ' ', p.marcaProduto, ' ', lt.qtProduto, um.siglaMedida) produto, \n";
            
            // loop mercados
            for(int i = 0; i < mercados.size(); i++){
                query += "SUM(IF(m.idMercado="+(i+1)+", pm.precoProduto,0)) As 'mercado"+(i+1)+"' ";
                if(i != mercados.size() - 1){
                    query += ",\n ";
                }
            }
            
            query += "\n FROM tbLista_has_tbProduto as lt\n" +
                    "INNER JOIN tbLista l ON l.idLista = lt.idLista\n" +
                    "INNER JOIN tbProduto p ON p.idProduto = lt.idProduto\n" +
                    "INNER JOIN tbUnidadeMedida um ON um.idMedida = p.idMedida\n" +
                    "INNER JOIN tbProduto_has_tbMercado pm ON pm.idProduto = p.idProduto AND pm.idStatusProduto = 1\n" +
                    "INNER JOIN tbMercado m ON m.idMercado = pm.idMercado\n" +
                    "WHERE l.idLista = "+idLista+"\n" +
                    "GROUP BY produto WITH ROLLUP \n" +
                    ") sub;";
            
            System.out.println("#######################");
            System.out.println("QUERY");
            System.out.println(query + "\n#######################");
            ResultSet result = stat.executeQuery(query);
            
            System.out.println("#######################");
            System.out.println(result.toString());
            System.out.println("#######################");
            
        }
        catch(Exception e){
            e.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
