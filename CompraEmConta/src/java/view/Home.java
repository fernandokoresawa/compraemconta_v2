package view;

import Model.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class Home extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        
        try {
        
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat1 = conexao.getConnection().createStatement();
            
            String select1 = 
                    "SELECT " +
                        "p.idProduto, " +
                        "p.nomeProduto, " +
                        "p.marcaProduto, " +
                        "p.descricaoProduto, " +
                        "p.fotoProduto, " +
                        "p.medidaProduto, " +
                        "u.siglaMedida, " +
                        "pm.precoProduto, " +
                        "s.nomeStatus, " +
                        "m.nomeMercado," +
                        "m.ruaMercado, " +
                        "b.nomeBairro "+
                    "FROM compraemconta.tbproduto_has_tbmercado as pm " +
                        "INNER JOIN tbproduto p ON p.idProduto = pm.idProduto " +
                        "INNER JOIN tbmercado m ON m.idMercado = pm.idMercado " +
                        "INNER JOIN tbstatusproduto s ON s.idStatus = pm.idStatusProduto " +
                        "INNER JOIN tbunidademedida u ON u.idMedida = p.idMedida " +
                        "INNER JOIN tbbairro b ON b.idBairro = m.idBairro " +
                    "ORDER BY " +
                        "p.nomeProduto; ";
                        
            ResultSet result1 = stat1.executeQuery(select1);
            
            ArrayList<Produto_Preco> produtos = new ArrayList<>();
            
            while (result1.next()) {
                Produto_Preco pp = new Produto_Preco();
                Produto p = new Produto();
                Mercado m = new Mercado();
                StatusProduto st = new StatusProduto();
                UnidadeMedida u = new UnidadeMedida();
                Bairro b = new Bairro();
                
                p.setIdProduto(result1.getInt(1));
                p.setNomeProduto(result1.getString(2));
                p.setMarcaProduto(result1.getString(3));
                p.setDescricaoProduto(result1.getString(4));
                p.setFotoProduto(result1.getString(5));
                p.setMedidaProduto(result1.getFloat(6));
                pp.setProduto(p);
                
                u.setSiglaMedida(result1.getString(7));
                pp.setUnidade(u);
                
                pp.setPrecoProduto(result1.getFloat(8));
                
                st.setNomeStatusProduto(result1.getString(9));
                pp.setStatusProduto(st);
                
                m.setNomeMercado(result1.getString(10));
                m.setRuaMercado(result1.getString(11));
                pp.setMercado(m);
                
                b.setNomeBairro(result1.getString(12));
                pp.setBairro(b);
                
                produtos.add(pp);
            }
            
            request.setAttribute("produtos", produtos);
            
            request.getRequestDispatcher("home.jsp").forward(request, response);
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
