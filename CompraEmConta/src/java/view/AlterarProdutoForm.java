package view;

import Model.Conexao;
import Model.Mercado;
import Model.Produto;
import Model.Produto_Preco;
import Model.StatusProduto;
import Model.UnidadeMedida;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class AlterarProdutoForm extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try{
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            
            int idProduto = Integer.parseInt(request.getParameter("idProduto"));
            int idMercado = Integer.parseInt(request.getParameter("idMercado"));
            
            
            
            // selecionar produto para preencher form de atualizacao
            java.sql.Statement stat1 = conexao.getConnection().createStatement();
            
            String select1 = 
                    "SELECT p.idProduto, p.nomeProduto, p.marcaProduto, p.descricaoProduto, p.medidaProduto, p.fotoProduto, u.siglaMedida, pm.precoProduto, s.nomeStatus, m.idMercado, m.nomeMercado " +
                    "FROM tbproduto_has_tbmercado as pm " +
                    "INNER JOIN tbproduto p ON p.idProduto = pm.idProduto " +
                    "INNER JOIN tbmercado m ON m.idMercado = pm.idMercado " +
                    "INNER JOIN tbstatusproduto s ON s.idStatus = pm.idStatusProduto " +
                    "INNER JOIN tbunidademedida u ON u.idMedida = p.idMedida " +
                    "WHERE p.idProduto = "+idProduto+" AND m.idMercado = "+idMercado+";";
        
            ResultSet result1 = stat1.executeQuery(select1);
            
            ArrayList<Produto_Preco> produtos = new ArrayList<>();
            
            while(result1.next()){
                Produto_Preco pp = new Produto_Preco();
                Produto produto = new Produto();
                Mercado mercado = new Mercado();
                StatusProduto status = new StatusProduto();
                UnidadeMedida unidade = new UnidadeMedida();
                produto.setIdProduto(result1.getInt(1));
                produto.setNomeProduto(result1.getString(2));
                produto.setMarcaProduto(result1.getString(3));
                produto.setDescricaoProduto(result1.getString(4));
                produto.setMedidaProduto(result1.getFloat(5));
                produto.setFotoProduto(result1.getString(6));
                pp.setProduto(produto);
                unidade.setSiglaMedida(result1.getString(7));
                pp.setUnidade(unidade);
                pp.setPrecoProduto(result1.getFloat(8));
                status.setNomeStatusProduto(result1.getString(9));
                pp.setStatusProduto(status);
                mercado.setIdMercado(result1.getInt(10));
                mercado.setNomeMercado(result1.getString(11));
                pp.setMercado(mercado);
                produtos.add(pp);
            
                //System.out.println(pp.getProduto().getNomeProduto());
            }
            
            request.setAttribute("produtos", produtos);
            
            
            // montar combo unidade medida
            java.sql.Statement stat2 = conexao.getConnection().createStatement();
            
            String query2 = "select idMedida, siglaMedida from tbunidademedida;";
            
            ResultSet result2 = stat2.executeQuery(query2);
            
            List<UnidadeMedida> medidas = new ArrayList<>();
            
            while(result2.next()){
                UnidadeMedida medida = new UnidadeMedida();
                medida.setIdMedida(result2.getInt(1));
                medida.setSiglaMedida(result2.getString(2));
                medidas.add(medida);
            }
            
            request.setAttribute("medidas", medidas);
            
            
            // montar combo status
            java.sql.Statement stat3 = conexao.getConnection().createStatement();
            
            String query3 = "select idStatus, nomeStatus from tbstatusproduto;";
            
            ResultSet result3 = stat3.executeQuery(query3);
            
            List<StatusProduto> status = new ArrayList<>();
            
            while(result3.next()){
                StatusProduto s = new StatusProduto();
                s.setIdStatusProduto(result3.getInt(1));
                s.setNomeStatusProduto(result3.getString(2));
                status.add(s);
            }
            
            request.setAttribute("status", status);
            
            
            // montar combo mercado
            java.sql.Statement stat4 = conexao.getConnection().createStatement();
            
            String query4 = "select idMercado, nomeMercado from tbMercado;";
            
            ResultSet result4 = stat4.executeQuery(query4);
            
            List<Mercado> mercados = new ArrayList<>();
            
            while(result4.next()){
                Mercado mercado = new Mercado();
                mercado.setIdMercado(result4.getInt(1));
                mercado.setNomeMercado(result4.getString(2));
                mercados.add(mercado);
            }
            
            request.setAttribute("mercados", mercados);
            
            
            request.getRequestDispatcher("alterarProduto.jsp").forward(request, response);
        
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
