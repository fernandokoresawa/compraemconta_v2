package view;

import Model.Conexao;
import Model.Mercado;
import Model.StatusProduto;
import Model.UnidadeMedida;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class CadastrarProdutoForm extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try{
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            
            // montar combo unidade medida
            java.sql.Statement stat1 = conexao.getConnection().createStatement();
            
            String query1 = "select idMedida, siglaMedida from tbunidademedida;";
            
            ResultSet result1 = stat1.executeQuery(query1);
            
            List<UnidadeMedida> medidas = new ArrayList<>();
            
            while(result1.next()){
                UnidadeMedida medida = new UnidadeMedida();
                medida.setIdMedida(result1.getInt(1));
                medida.setSiglaMedida(result1.getString(2));
                medidas.add(medida);
            }
            
            request.setAttribute("medidas", medidas);
            
            
            // montar combo status
            java.sql.Statement stat2 = conexao.getConnection().createStatement();
            
            String query2 = "select idStatus, nomeStatus from tbstatusproduto;";
            
            ResultSet result2 = stat2.executeQuery(query2);
            
            List<StatusProduto> status = new ArrayList<>();
            
            while(result2.next()){
                StatusProduto s = new StatusProduto();
                s.setIdStatusProduto(result2.getInt(1));
                s.setNomeStatusProduto(result2.getString(2));
                status.add(s);
            }
            
            request.setAttribute("status", status);
            
            
            // montar combo mercado
            java.sql.Statement stat3 = conexao.getConnection().createStatement();
            
            String query3 = "select idMercado, nomeMercado, ruaMercado from tbMercado order by nomeMercado;";
            
            ResultSet result3 = stat3.executeQuery(query3);
            
            List<Mercado> mercados = new ArrayList<>();
            
            while(result3.next()){
                Mercado mercado = new Mercado();
                mercado.setIdMercado(result3.getInt(1));
                mercado.setNomeMercado(result3.getString(2));
                mercado.setRuaMercado(result3.getString(3));
                mercados.add(mercado);
            }
            
            request.setAttribute("mercados", mercados);
            
            
            request.getRequestDispatcher("cadastrarProduto.jsp").forward(request, response);
        
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
