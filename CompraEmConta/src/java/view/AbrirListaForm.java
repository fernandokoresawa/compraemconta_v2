package view;

import Model.Conexao;
import Model.Lista;
import Model.Lista_Produto;
import Model.Produto;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class AbrirListaForm extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            
            int idLista = Integer.parseInt(request.getParameter("idLista"));
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String query = 
                "SELECT l.idLista, l.nomeLista, p.nomeProduto, lp.qtProduto " +
                "FROM tblista_has_tbproduto lp " +
                "INNER JOIN tblista l ON l.idLista = lp.idLista " +
                "INNER JOIN tbproduto p ON p.idProduto = lp.idProduto " +
                "WHERE l.idLista = "+idLista+";";
            
            ResultSet result = stat.executeQuery(query);
            
            ArrayList<Lista> listas = new ArrayList<>();
            
            String nomeLista = null;
            
            while(result.next()){
                Lista l = new Lista();
                Lista nL = new Lista();
                Produto p = new Produto();
                Lista_Produto lp = new Lista_Produto();
                l.setIdLista((result.getInt(1)));
                
                nomeLista = result.getString(2);
                
                p.setNomeProduto(result.getString(3));
                l.setProdutos(p);
                
                lp.setQuantidadeProduto(result.getInt(4));
                l.setLp(lp);
                
                listas.add(l);
            }
            //System.out.println(listas);
            request.setAttribute("listas", listas);
            
            request.setAttribute("nomeLista", nomeLista);
            
            request.setAttribute("idLista", idLista);
            System.out.println("IDLISTA---------->"+idLista);
                    
            request.getRequestDispatcher("abrirLista.jsp").forward(request, response);
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
