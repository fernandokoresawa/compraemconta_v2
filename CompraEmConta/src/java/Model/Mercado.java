package Model;

import java.sql.SQLException;

/**
 *
 * @author Fernando
 */
public class Mercado {
    
    // ATRIBUTOS
    private int idMercado;
    private String nomeMercado;
    private String ruaMercado;
    private String latitudeMercado;
    private String longitudeMercado;
    private Usuario usuario;
    private Bairro bairro;
    private Cidade cidade;
    private Estado estado;
    
    //GETTERS E SETTERS

    public int getIdMercado() {
        return idMercado;
    }

    public void setIdMercado(int idMercado) {
        this.idMercado = idMercado;
    }

    public String getNomeMercado() {
        return nomeMercado;
    }

    public void setNomeMercado(String nomeMercado) {
        this.nomeMercado = nomeMercado;
    }

    public String getRuaMercado() {
        return ruaMercado;
    }

    public void setRuaMercado(String ruaMercado) {
        this.ruaMercado = ruaMercado;
    }

    public String getLatitudeMercado() {
        return latitudeMercado;
    }

    public void setLatitudeMercado(String latitudeMercado) {
        this.latitudeMercado = latitudeMercado;
    }

    public String getLongitudeMercado() {
        return longitudeMercado;
    }

    public void setLongitudeMercado(String longitudeMercado) {
        this.longitudeMercado = longitudeMercado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Bairro getBairro() {
        return bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
    public void inserirMercado() {
        
        try {
            
            Conexao conexao =  new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String query = 
                "INSERT INTO compraemconta.tbMercado (nomeMercado, ruaMercado, latitudeMercado, longitudeMercado, idBairro, idUsuario) VALUES ('"+nomeMercado+"','"+ruaMercado+"','"+latitudeMercado+"','"+longitudeMercado+"',"+bairro.getIdBairro()+","+usuario.getIdUsuario()+")";
            
            stat.executeUpdate(query);
            
            stat.close();
            conexao.getConnection().close();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    public void excluirMercado(){
        
        try {
         
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String delete = "DELETE FROM tbmercado WHERE idMercado = "+idMercado+";";
            
            stat.executeUpdate(delete);
            
            stat.close();
            conexao.getConnection().close();
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    public void atualizarMercado(){
        
        try {
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String update = 
                "UPDATE " +
                    "tbmercado " +
                "SET " +
                    "nomemercado = '"+nomeMercado+"', " +
                    "ruamercado = '"+ruaMercado+"', " +
                    "latitudeMercado = '"+latitudeMercado+"', " +
                    "longitudemercado = '"+longitudeMercado+"', " +
                    "idusuario = "+usuario.getIdUsuario()+", " +
                    "idbairro = "+bairro.getIdBairro()+" " +
                "WHERE " +
                    "idmercado = "+idMercado+"";
            
            stat.executeUpdate(update);
            
            stat.close();
            conexao.getConnection().close();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
            
        
    }
    
    public void listarMercados(String nomeMercado){
        try {
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String query = 
                "SELECT " +
                    "m.idMercado, " +
                    "m.nomeMercado, " +
                    "m.ruaMercado, " +
                    "b.nomeBairro, " +
                    "c.nomeCidade, " +
                    "e.siglaEstado " +
                "FROM tbmercado m " +
                "INNER JOIN tbbairro b ON b.idBairro = m.idBairro " +
                "INNER JOIN tbcidade c ON c.idCidade = b.idCidade " +
                "INNER JOIN tbestado e ON e.idEstado = c.idEstado " +
                "WHERE m.idMercado = '"+nomeMercado+"' " +
                "ORDER BY m.nomeMercado;";
            
            stat.close();
            conexao.getConnection().close();
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
