package Model;

import java.util.List;

/**
 *
 * @author Fernando
 */
public class Cidade {
   
    private int idCidade;
    private String nomeCidade;
    private List<Mercado> mercado;
    private Estado estado;
    
    // GETTERS E SETTERS
    public int getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(int idCidade) {
        this.idCidade = idCidade;
    }

    public String getNomeCidade() {
        return nomeCidade;
    }

    public void setNomeCidade(String nomeCidade) {
        this.nomeCidade = nomeCidade;
    }

    public List<Mercado> getMercado() {
        return mercado;
    }

    public void setMercado(List<Mercado> mercado) {
        this.mercado = mercado;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
    public int toInt(){
        return this.idCidade;
    }
    
}
