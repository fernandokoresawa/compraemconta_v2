package Model;

import java.util.List;

/**
 *
 * @author Fernando
 */
public class Bairro {

    private int idBairro;
    private String nomeBairro;
    private List<Mercado> mercado;
    private Cidade cidade;

    public int getIdBairro() {
        return idBairro;
    }

    public void setIdBairro(int idBairro) {
        this.idBairro = idBairro;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Mercado> getMercado() {
        return mercado;
    }

    public void setMercado(List<Mercado> mercado) {
        this.mercado = mercado;
    }
    
    
}
