package Model;

/**
 *
 * @author Fernando
 */
public class Lista_Produto {
    
    private int idLista_Produto;
    private Lista lista;
    private Produto produto;
    private int quantidadeProduto;
    
    public int getIdLista_Produto() {
        return idLista_Produto;
    }

    public void setIdLista_Produto(int idLista_Produto) {
        this.idLista_Produto = idLista_Produto;
    }

    public Lista getLista() {
        return lista;
    }

    public void setLista(Lista lista) {
        this.lista = lista;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidadeProduto() {
        return quantidadeProduto;
    }

    public void setQuantidadeProduto(int quantidadeProduto) {
        this.quantidadeProduto = quantidadeProduto;
    }
    
    public void salvar(){
        
        try{
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement transacao = conexao.getConnection().createStatement();
            
            String query =
                    "INSERT INTO tbLista_has_tbProduto (intIdLista, intIdProduto, intQuantidade)"
                    + "VALUES ("+lista.getIdLista()+", "+produto.getIdProduto()+", "+quantidadeProduto+")";
            
            transacao.executeUpdate(query);
            
            transacao.close();
            
            conexao.getConnection().close();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
            
    }
    
}
