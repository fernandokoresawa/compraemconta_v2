package Model;

/**
 *
 * @author Fernando
 */
public class Produto_Preco {
    
    // ATRIBUTOS
    private int idProduto_preco;
    private Produto produto;
    private Mercado mercado;
    private float precoProduto;
    private StatusProduto statusProduto;
    private UnidadeMedida unidade;
    private Bairro bairro;

    // GETTERS E SETTERS
    public int getIdProduto_preco() {
        return idProduto_preco;
    }

    public void setIdProduto_preco(int idProduto_preco) {
        this.idProduto_preco = idProduto_preco;
    }
    
    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public float getPrecoProduto() {
        return precoProduto;
    }

    public void setPrecoProduto(float precoProduto) {
        this.precoProduto = precoProduto;
    }

    public StatusProduto getStatusProduto() {
        return statusProduto;
    }

    public void setStatusProduto(StatusProduto statusProduto) {
        this.statusProduto = statusProduto;
    }
           
    public UnidadeMedida getUnidade() {
        return unidade;
    }

    public void setUnidade(UnidadeMedida unidade) {
        this.unidade = unidade;
    }

    public Bairro getBairro() {
        return bairro;
    }

    public void setBairro(Bairro bairro) {
        this.bairro = bairro;
    }
    
}