package Model;

/**
 *
 * @author Fernando
 */
public class UnidadeMedida {

    private int idMedida;
    private String nomeMedida;
    private String siglaMedida;
    
    public int getIdMedida() {
        return idMedida;
    }

    public void setIdMedida(int idMedida) {
        this.idMedida = idMedida;
    }

    public String getNomeMedida() {
        return nomeMedida;
    }

    public void setNomeMedida(String nomeMedida) {
        this.nomeMedida = nomeMedida;
    }

    public String getSiglaMedida() {
        return siglaMedida;
    }

    public void setSiglaMedida(String siglaMedida) {
        this.siglaMedida = siglaMedida;
    }
        
}
