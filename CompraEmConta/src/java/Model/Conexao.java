package Model;

import java.sql.*;

/**
 *
 * @author Fernando
 */
public class Conexao {
    
    public Connection getConnection() throws ClassNotFoundException {
        
        Connection conexao;
        String driver = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/compraemconta";
        String login = "root";
        String senha = "Compra!@#";
        
        try {
            Class.forName(driver);
            conexao = DriverManager.getConnection(url, login, senha);
        }
        catch(SQLException e) {
            throw new RuntimeException(e);
        }
        return conexao;
    }
    
}
