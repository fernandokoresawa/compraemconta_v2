package Model;

import java.util.List;

/**
 *
 * @author Fernando
 */
public class StatusProduto {
    
    private int idStatusProduto;
    private String nomeStatusProduto;
    private List<Produto> produto;

    // GETTERS E SETTERS
    public int getIdStatusProduto() {
        return idStatusProduto;
    }

    public void setIdStatusProduto(int idStatusProduto) {
        this.idStatusProduto = idStatusProduto;
    }

    public String getNomeStatusProduto() {
        return nomeStatusProduto;
    }

    public void setNomeStatusProduto(String nomeStatusProduto) {
        this.nomeStatusProduto = nomeStatusProduto;
    }

    public List<Produto> getProduto() {
        return produto;
    }

    public void setProduto(List<Produto> produto) {
        this.produto = produto;
    }
    
}
