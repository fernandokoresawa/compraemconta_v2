package Model;

/**
 *
 * @author Fernando
 */
public class Produto {
    
    // ATRIBUTOS
    private int idProduto;
    private String nomeProduto;
    private String marcaProduto;
    private float medidaProduto;
    private String descricaoProduto;
    // VER ISSO ---------------------------------------------
    private String fotoProduto;
    private Usuario usuario;
    private UnidadeMedida medida;
    
    
    // GETTERS E SETTERS
    
    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getMarcaProduto() {
        return marcaProduto;
    }

    public void setMarcaProduto(String marcaProduto) {
        this.marcaProduto = marcaProduto;
    }
    
    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public float getMedidaProduto() {
        return medidaProduto;
    }

    public void setMedidaProduto(float medidaProduto) {
        this.medidaProduto = medidaProduto;
    }
    
    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public String getFotoProduto() {
        return fotoProduto;
    }

    public void setFotoProduto(String fotoProduto) {
        this.fotoProduto = fotoProduto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UnidadeMedida getMedida() {
        return medida;
    }

    public void setMedida(UnidadeMedida medida) {
        this.medida = medida;
    }
    
}
