package Model;

import java.util.List;

/**
 *
 * @author Fernando
 */
public class Lista {
    
    private int idLista;
    private String nomeLista;
    private int nItens;
    private Produto produtos;
    private Lista_Produto lp;
    private Usuario usuario;
    
    // GETTERS E SETTERS
    public int getIdLista() {
        return idLista;
    }

    public void setIdLista(int idLista) {
        this.idLista = idLista;
    }

    public String getNomeLista() {
        return nomeLista;
    }

    public void setNomeLista(String nomeLista) {
        this.nomeLista = nomeLista;
    }

    public Produto getProdutos() {
        return produtos;
    }

    public void setProdutos(Produto produtos) {
        this.produtos = produtos;
    }
  
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }    

    public int getnItens() {
        return nItens;
    }

    public void setnItens(int nItens) {
        this.nItens = nItens;
    }

    public Lista_Produto getLp() {
        return lp;
    }

    public void setLp(Lista_Produto lp) {
        this.lp = lp;
    }
    
}
