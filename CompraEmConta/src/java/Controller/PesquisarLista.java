package Controller;

import Model.Conexao;
import Model.Lista;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class PesquisarLista extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            
            String nomeL = request.getParameter("nomeLista");
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String query = 
                "SELECT l.idLista, l.nomeLista " +
                "FROM tblista l " +
                "WHERE l.nomeLista like '"+nomeL+"';";
            
            ResultSet result = stat.executeQuery(query);
            
            ArrayList<Lista> listas = new ArrayList<>();
            
            while(result.next()){
                Lista l = new Lista();
                l.setIdLista(result.getInt(1));
                l.setNomeLista(result.getString(2));
                listas.add(l);
            }
            
            request.setAttribute("listas", listas);
            
            request.getRequestDispatcher("resultadoLista.jsp").forward(request, response);
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
