package Controller;

import Model.Conexao;
import Model.Mercado;
import Model.Produto;
import Model.Produto_Preco;
import Model.StatusProduto;
import Model.UnidadeMedida;
import Model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class AlterarProduto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try{
            
            int idProd = Integer.parseInt(request.getParameter("idProduto"));
            int idMerc = Integer.parseInt(request.getParameter("idMercado"));
            
            String nomeP = request.getParameter("nomeProduto");
            String marcaP = request.getParameter("marcaProduto");
            Float medida = Float.parseFloat(request.getParameter("medidaProduto"));
            String descricaoP = request.getParameter("descricaoProduto");
            String fotoP = request.getParameter("fotoProduto");
            int idMedida = Integer.parseInt(request.getParameter("selectMedida"));
            
            Usuario usuario = new Usuario();
            usuario.setIdUsuario(1);
            
            UnidadeMedida unidadeM = new UnidadeMedida();
            unidadeM.setIdMedida(idMedida);
            
            // update produto -> tbproduto
            int idProduto = alterarProduto(nomeP, marcaP, medida, descricaoP, fotoP, idMedida, idProd);
            
            //System.out.println("###### "+ idProduto);
            
            
            // update produto - mercado - preco
            float precoP = Float.parseFloat(request.getParameter("precoProduto"));
            int idStatus = Integer.parseInt(request.getParameter("selectStatus"));
            int idMercado = Integer.parseInt(request.getParameter("selectMercado"));
            
            Mercado mercado = new Mercado();
            mercado.setIdMercado(idMercado);
            
            StatusProduto status = new StatusProduto();
            status.setIdStatusProduto(idStatus);
            
            alterarProdutoMercado(precoP, idProd, idMercado, idStatus, idMerc, idProd);
            
            if(!mercado.equals("")){
                String msgSucesso = "Produto alterado com sucesso!";
                request.setAttribute("msgSucesso", msgSucesso);
                request.getRequestDispatcher("PesquisarProdutoForm").forward(request, response);
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    private int alterarProduto(String nomeP, String marcaP, Float medida, String descricaoP, String fotoP, int idMedida, int idProd) {
        
        try {
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat1 = conexao.getConnection().createStatement();
            
            String query1 = 
                    "UPDATE tbproduto " +
                    "SET " +
                        "nomeProduto = '"+nomeP+"', " +
                        "marcaProduto = '"+marcaP+"', " +
                        "medidaProduto = '"+medida+"', " +
                        "descricaoProduto = '"+descricaoP+"', " +
                        "fotoProduto = '"+fotoP+"', " +
                        "idMedida = "+idMedida+" " +
                    "WHERE " +
                        "idProduto = "+idProd+";";
            
            stat1.executeUpdate(query1, Statement.RETURN_GENERATED_KEYS);
                
            try (ResultSet generatedKeys = stat1.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                }
                else {
                    throw new Exception("Failed!");
                }
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        return -1;
    }

    private void alterarProdutoMercado(float precoP, int idProduto, int idMercado, int idStatus, int idMerc, int idProd) {
        
        try{
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat2 = conexao.getConnection().createStatement();
            
            String query2 = 
                    "UPDATE tbproduto_has_tbmercado " +
                    "SET " +
                        "precoProduto = "+precoP+", " +
                        "idMercado = "+idMercado+", " +
                        "idStatusProduto = "+idStatus+" " +
                    "WHERE " +
                        "idProduto = "+idProduto+" and idMercado = "+idMerc+";";
            
            stat2.executeUpdate(query2);
            
            //System.out.println("INSERT -> ##" + insert);
            
            stat2.close();
            conexao.getConnection().close();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
