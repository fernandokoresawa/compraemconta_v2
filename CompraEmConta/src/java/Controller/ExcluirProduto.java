package Controller;

import Model.Conexao;
import Model.Mercado;
import Model.Produto;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class ExcluirProduto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            
            int idProduto = Integer.parseInt(request.getParameter("idProduto"));
            int idMercado = Integer.parseInt(request.getParameter("idMercado"));
            
            excluirProduto(idProduto, idMercado);
     
            if(idProduto != 0 && idMercado != 0){
                String msgSucesso = "Produto excluído com sucesso!";
                request.setAttribute("msgSucesso", msgSucesso);
                request.getRequestDispatcher("PesquisarProdutoForm").forward(request, response);
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void excluirProduto(int idProduto, int idMercado) {
        
        try {
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
        
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String query = 
                    "delete from tbproduto_has_tbmercado where idProduto = "+idProduto+" and idMercado = "+idMercado+";";
            
            stat.executeUpdate(query);
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
