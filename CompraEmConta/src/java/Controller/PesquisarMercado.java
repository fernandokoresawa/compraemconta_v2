package Controller;

import Model.Bairro;
import Model.Cidade;
import Model.Conexao;
import Model.Estado;
import Model.Mercado;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class PesquisarMercado extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            
            String nomeM = request.getParameter("nomeMercado");
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String query = 
                "SELECT " +
                    "m.idMercado, " +
                    "m.nomeMercado, " +
                    "m.ruaMercado, " +
                    "b.nomeBairro, " +
                    "c.nomeCidade, " +
                    "e.siglaEstado " +
                "FROM tbmercado m " +
                "INNER JOIN tbbairro b ON b.idBairro = m.idBairro " +
                "INNER JOIN tbcidade c ON c.idCidade = b.idCidade " +
                "INNER JOIN tbestado e ON e.idEstado = c.idEstado " +
                "WHERE m.nomeMercado = '"+nomeM+"' " +
                "ORDER BY m.nomeMercado;";
            
            ResultSet result = stat.executeQuery(query);
            ArrayList<Mercado> mercados = new ArrayList<>();
            
            while(result.next()){
                Mercado m = new Mercado();
                Bairro b = new Bairro();
                Cidade c = new Cidade();
                Estado e = new Estado();
                
                m.setIdMercado(result.getInt(1));
                m.setNomeMercado(result.getString(2));
                m.setRuaMercado(result.getString(3));
                
                b.setNomeBairro(result.getString(4));
                m.setBairro(b);
                
                c.setNomeCidade(result.getString(5));
                m.setCidade(c);
                
                e.setNomeEstado(result.getString(6));
                m.setEstado(e);
                
                mercados.add(m);
            }
            request.setAttribute("mercados", mercados);
            request.getRequestDispatcher("resultadoMercado.jsp").forward(request, response);
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
