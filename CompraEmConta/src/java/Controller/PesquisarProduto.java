package Controller;

import Model.Conexao;
import Model.Mercado;
import Model.Produto;
import Model.Produto_Preco;
import Model.StatusProduto;
import Model.UnidadeMedida;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class PesquisarProduto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
        
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            String nomeP = request.getParameter("nomeProduto");
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String select = 
                    "SELECT p.idProduto, p.nomeProduto, p.marcaProduto, p.descricaoProduto, p.medidaProduto, u.siglaMedida, pm.precoProduto, s.nomeStatus, m.idMercado, m.nomeMercado " +
                    "FROM tbproduto_has_tbmercado as pm " +
                    "INNER JOIN tbproduto p ON p.idProduto = pm.idProduto " +
                    "INNER JOIN tbmercado m ON m.idMercado = pm.idMercado " +
                    "INNER JOIN tbstatusproduto s ON s.idStatus = pm.idStatusProduto " +
                    "INNER JOIN tbunidademedida u ON u.idMedida = p.idMedida " +
                    "WHERE p.nomeProduto = '"+nomeP+"' " +
                    "ORDER BY p.nomeProduto;";
        
            ResultSet result = stat.executeQuery(select);
            
            ArrayList<Produto_Preco> produtos = new ArrayList<>();
            //Produto produto = new Produto();
            //Mercado mercado = new Mercado();
            //StatusProduto status = new StatusProduto();
            
            while(result.next()){
                Produto_Preco pp = new Produto_Preco();
                Produto produto = new Produto();
                Mercado mercado = new Mercado();
                StatusProduto status = new StatusProduto();
                UnidadeMedida unidade = new UnidadeMedida();
                produto.setIdProduto(result.getInt(1));
                produto.setNomeProduto(result.getString(2));
                produto.setMarcaProduto(result.getString(3));
                produto.setDescricaoProduto(result.getString(4));
                produto.setMedidaProduto(result.getFloat(5));
                pp.setProduto(produto);
                unidade.setSiglaMedida(result.getString(6));
                pp.setUnidade(unidade);
                pp.setPrecoProduto(result.getFloat(7));
                status.setNomeStatusProduto(result.getString(8));
                pp.setStatusProduto(status);
                mercado.setIdMercado(result.getInt(9));
                mercado.setNomeMercado(result.getString(10));
                pp.setMercado(mercado);
                /*int id = result.getInt(1);
                String prod = result.getString(2);
                String marcaP = result.getString(3);
                String precoP = result.getString(4);
                String statusP = result.getString(5);
                String merc = result.getString(6);*/
                produtos.add(pp);
            
                //System.out.println(pp.getProduto().getNomeProduto());
            }
            
            request.setAttribute("produtos", produtos);
            
            request.getRequestDispatcher("resultadoProdutos.jsp").forward(request, response);
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
