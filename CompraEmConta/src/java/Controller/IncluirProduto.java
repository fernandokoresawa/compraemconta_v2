package Controller;

import Model.Conexao;
import Model.Mercado;
import Model.StatusProduto;
import Model.UnidadeMedida;
import Model.Usuario;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Fernando
 */
public class IncluirProduto extends HttpServlet {

    /*private boolean isMultipart;
    private String filePath;
    private int maxFileSize = 50 * 1024;
    private int maxMemSize = 4 * 1024;
    private File file ;
    
    public void init( ){
      // Get the file location where it would be stored.
      filePath = 
             getServletContext().getInitParameter("file-upload"); 
    }*/
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        /*
        DiskFileItemFactory factory = new DiskFileItemFactory();
        
        factory.setSizeThreshold(maxMemSize);
        
        factory.setRepository(new File(""));

        
        ServletFileUpload upload = new ServletFileUpload(factory);
        
        upload.setSizeMax( maxFileSize );
        */
        try{
        
            // -------FOTO
            
            /*
            List fileItems = upload.parseRequest(request);

            
            Iterator i = fileItems.iterator();

            
            while (i.hasNext()) {
            
                FileItem fi = (FileItem)i.next();
                if (!fi.isFormField()) {
                    // Get the uploaded file parameters
                    String fieldName = fi.getFieldName();
                    String fileName = fi.getName();
                    String contentType = fi.getContentType();
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();
                    // Write the file
                    if( fileName.lastIndexOf("\\") >= 0 ){
                        file = new File( filePath + 
                        fileName.substring( fileName.lastIndexOf("\\"))) ;
                    }
                    else {
                        file = new File( filePath + 
                        fileName.substring(fileName.lastIndexOf("\\")+1)) ;
                    }
                    fi.write( file ) ;
                    System.out.println("Uploaded Filename: " + fileName + "<br>");
                
                    String caminho = file.getAbsolutePath();
                    System.out.println("IMAGEEEEEEEEEEEM: "+caminho);
                }
            }*/
            // -------FOTO
            
            
            String nomeP = request.getParameter("nomeProduto");
            String marcaP = request.getParameter("marcaProduto");
            Float medida = Float.parseFloat(request.getParameter("medidaProduto"));
            String descricaoP = request.getParameter("descricaoProduto");
            String fotoP = request.getParameter("fotoProduto");
            int idMedida = Integer.parseInt(request.getParameter("selectMedida"));
            
            Usuario usuario = new Usuario();
            usuario.setIdUsuario(1);
            
            UnidadeMedida unidadeM = new UnidadeMedida();
            unidadeM.setIdMedida(idMedida);
            
            // insert produto -> tbproduto
            int idProduto = inserirProduto(nomeP, marcaP, medida, descricaoP, fotoP, usuario, unidadeM);
            
            //System.out.println("###### "+ idProduto);
            
            
            // associar produto - mercado - preco
            float precoP = Float.parseFloat(request.getParameter("precoProduto"));
            int idStatus = Integer.parseInt(request.getParameter("selectStatus"));
            int idMercado = Integer.parseInt(request.getParameter("selectMercado"));
            
            Mercado mercado = new Mercado();
            mercado.setIdMercado(idMercado);
            
            StatusProduto status = new StatusProduto();
            status.setIdStatusProduto(idStatus);
            
            associarProdutoMercado(precoP, idProduto, idMercado, idStatus, usuario);
            
            if(!mercado.equals("")){
                String msgSucesso = "Produto gravado com sucesso!";
                request.setAttribute("msgSucesso", msgSucesso);
                request.getRequestDispatcher("CadastrarProdutoForm").forward(request, response);
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private int inserirProduto(String nomeP, String marcaP, Float medida, String descricaoP, String fotoP, Usuario usuario, UnidadeMedida unidadeM) {
        
        try {
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat1 = conexao.getConnection().createStatement();
            
            String query1 = 
                    "insert into tbproduto (nomeProduto, marcaProduto, medidaProduto, descricaoProduto, fotoProduto, idUsuario, idMedida) VALUES ('"+nomeP+"', '"+marcaP+"', "+medida+", '"+descricaoP+"', '"+fotoP+"', "+usuario.getIdUsuario()+", "+unidadeM.getIdMedida()+" );";
            
            stat1.executeUpdate(query1, Statement.RETURN_GENERATED_KEYS);
                
            try (ResultSet generatedKeys = stat1.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                }
                else {
                    throw new Exception("Failed!");
                }
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        return -1;
    }

    private void associarProdutoMercado(float precoP, int idProduto, int idMercado, int idStatus, Usuario usuario) {
        
        try{
            
            Conexao conexao = new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat2 = conexao.getConnection().createStatement();
            
            String query2 = 
                    "insert into tbproduto_has_tbmercado (precoProduto, idProduto, idMercado, idStatusProduto, idUsuario) "
                    + "VALUES ( "+precoP+", "+idProduto+", "+idMercado+", "+idStatus+", "+usuario.getIdUsuario()+");";
            
            int insert = stat2.executeUpdate(query2);
            
            System.out.println("INSERT -> ##" + insert);
            
            stat2.close();
            conexao.getConnection().close();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }

}
