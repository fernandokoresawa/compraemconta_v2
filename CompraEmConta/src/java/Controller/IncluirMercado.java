package Controller;

import Model.Bairro;
import Model.Cidade;
import Model.Conexao;
import Model.Mercado;
import Model.Usuario;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.*;

/**
 *
 * @author Fernando
 */
public class IncluirMercado extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
                        
            int idBairro = Integer.parseInt(request.getParameter("selectBairro"));
            
            Bairro bairro = new Bairro();
            bairro.setIdBairro(idBairro);

            Usuario usuario = new Usuario();
            usuario.setIdUsuario(1);
            
            String nomeMercado = request.getParameter("nomeMercado");
            String ruaMercado = request.getParameter("ruaMercado");
            String latitudeMercado = request.getParameter("latitudeMercado");
            String longitudeMercado = request.getParameter("longitudeMercado");
           
            //  inserir mercado no banco
            //inserirMercadoNoBanco(nomeMercado, ruaMercado, latitudeMercado, longitudeMercado, bairro, usuario);
            

            if(!longitudeMercado.trim().equals("")){
                String msgSucesso = "Mercado incluído com SUCESSO!";
                request.setAttribute("msgSucesso", msgSucesso);
                request.getRequestDispatcher("cadastrarMercado.jsp").forward(request, response);
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }
    
    
    private void inserirMercadoNoBanco(String nomeMercado, String ruaMercado, String latitudeMercado, String longitudeMercado, Bairro bairro, Usuario usuario) {
        
        try {
            
            Conexao conexao =  new Conexao();
            conexao.getConnection();
            
            java.sql.Statement stat = conexao.getConnection().createStatement();
            
            String query = 
                "INSERT INTO compraemconta.tbMercado (nomeMercado, ruaMercado, latitudeMercado, longitudeMercado, idBairro, idUsuario) VALUES ('"+nomeMercado+"','"+ruaMercado+"','"+latitudeMercado+"','"+longitudeMercado+"',"+bairro.getIdBairro()+","+usuario.getIdUsuario()+")";
            
            stat.executeUpdate(query);
            
            stat.close();
            conexao.getConnection().close();
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
