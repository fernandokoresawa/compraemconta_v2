package Controller;

import Model.Conexao;
import Model.Produto;
import Model.Usuario;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class IncluirLista_Produto extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try{
            
            String nomeLista = request.getParameter("nomeLista");
            String[] prods = request.getParameterValues("nomeProduto");
            String[] qtProd = request.getParameterValues("qtProduto");
                                
            Usuario usuario = new Usuario();
            usuario.setIdUsuario(1);
            
            //String[] prods = request.getParameterValues("produtos");
                        
            //inserir os produtos que nao existem
            incluirProdutosNaoEncontrados(prods, usuario);
            
            //recuperar o id de todos os produtos
            List<Integer> idsprodutos = buscaridsProdutos(prods);
            
            /*for(Integer i : idsprodutos) {
                System.out.println("id: " + i);
            }*/
            
            //inserir a lista no banco
            int idLista = insereListaNoBanco(nomeLista, usuario);
            
            //System.out.println("idLista:" + idLista);
            
            //associar os produtos a lista
            associaProdutosALista(idsprodutos, idLista, qtProd);
                                    
            request.getRequestDispatcher("sucesso.jsp").forward(request, response);
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void incluirProdutosNaoEncontrados(String[] prods, Usuario usuario) {
        
        try{
            for(String nome : prods){

                Conexao conexao = new Conexao();
                conexao.getConnection();

                java.sql.Statement statement = conexao.getConnection().createStatement();

                // ver se ja tem no banco select
                String query = "select nomeProduto from tbproduto where nomeProduto = '"+nome+"'"
                        + " GROUP BY nomeProduto";

                ResultSet result = statement.executeQuery(query);
                
                if(!result.next()){
                    Produto produto = new Produto();
                    produto.setNomeProduto(nome);
                    
                    query = "Insert into tbproduto (nomeProduto, idUsuario) Values('" + produto.getNomeProduto() + "',"+usuario.getIdUsuario()+")";
                    statement.executeUpdate(query);
                }         

            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
    }   

    private List<Integer> buscaridsProdutos(String[] prods) {
      
        try{
            Conexao conexao = new Conexao();
            conexao.getConnection();

            java.sql.Statement statement = conexao.getConnection().createStatement();

            // ver se ja tem no banco select
            String query = "select idProduto from tbproduto where nomeProduto in ("+joinString(prods)+")"
                    + " GROUP BY nomeProduto";

            //System.out.println("quer: " + query);

            ResultSet res = statement.executeQuery(query);

            List<Integer> result = new ArrayList<>();
            
            while(res.next()){
                result.add(res.getInt("idProduto"));
            }
        
            return result;
            
        }
      
        catch(Exception e){
            e.printStackTrace();
        }
        
        return null;
        
    }
    
    private String joinString(String[] array) {
        
        String result = "";
        
        for (int i = 0; i < array.length; i++) {
            if(!array[i].trim().equals("")){
               result += "'" + array[i] + "',";
            }
        }
        
        result = result.substring(0,result.lastIndexOf(","));
        
        return result;
        
    }

    private int insereListaNoBanco(String nomeLista, Usuario usuario) {
    
        try{
            Conexao conexao = new Conexao();
            conexao.getConnection();

            java.sql.Statement statement = conexao.getConnection().createStatement();
            String query = "Insert Into TbLista(nomeLista, idUsuario) Values('" + nomeLista + "'," + usuario.getIdUsuario() + ")" ;
            
            //System.out.println("quer: " + query);
     
            statement.executeUpdate(query, Statement.RETURN_GENERATED_KEYS);
                
            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getInt(1);
                }
                else {
                    throw new Exception("Failed!");
                }
            }
        }
      
        catch(Exception e){
            e.printStackTrace();
        }
       
        return -1;
    }

    private void associaProdutosALista(List<Integer> idsprodutos, int idLista, String[] qtProd) {
        try{
            Conexao conexao = new Conexao();
            conexao.getConnection();

            java.sql.Statement statement = conexao.getConnection().createStatement();
            
            int i=0;
            for(Integer id : idsprodutos){
                String query = "Insert Into tbLista_Has_TbProduto(idLista, idProduto, qtProduto) "
                    + "Values(" + idLista + "," + id + ", "+qtProd[i]+")" ;
                i++;
                //System.out.println("quer: " + query);
     
                statement.executeUpdate(query);
            }
        }
      
        catch(Exception e){
            e.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
