package Controller;

import Model.Bairro;
import Model.Mercado;
import Model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Fernando
 */
public class AlterarMercado extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try {
            
            int idMercado = Integer.parseInt(request.getParameter("idMercado"));
            String nomeMercado = request.getParameter("nomeMercado");
            String ruaMercado = request.getParameter("ruaMercado");
            String latitudeMercado = request.getParameter("latitudeMercado");
            String longitudeMercado = request.getParameter("longitudeMercado");
            int idBairro = Integer.parseInt(request.getParameter("selectBairro"));
            int idUsuario = 1;
            
            Mercado m = new Mercado();
            Bairro b = new Bairro();
            Usuario u = new Usuario();
            
            m.setIdMercado(idMercado);
            m.setNomeMercado(nomeMercado);
            m.setRuaMercado(ruaMercado);
            m.setLatitudeMercado(latitudeMercado);
            m.setLongitudeMercado(longitudeMercado);
            b.setIdBairro(idBairro);
            u.setIdUsuario(idUsuario);
            m.setBairro(b);
            m.setUsuario(u);
            m.atualizarMercado();
            
            if(idMercado != 0){
                m.listarMercados(nomeMercado);
                request.setAttribute("nomeMercado", nomeMercado);
                String msgSucesso = "Mercado alterado com sucesso!";
                request.setAttribute("msgSucesso", msgSucesso);
                request.getRequestDispatcher("PesquisarMercado").forward(request, response);
            }
            
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
